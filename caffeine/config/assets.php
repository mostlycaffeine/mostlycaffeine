<?php
/**
 * Set the styles and scripts
 *
 * @package caffeinebuilt
 */

if ( ! function_exists( 'cb_assets' ) ) :

	function cb_async_scripts( $url ) {
		if ( strpos( $url, '#asyncload' ) === false ) {
			return $url;
		} elseif ( is_admin() ) {
			return str_replace( '#asyncload', '', $url );
		} else {
			return str_replace( '#asyncload', '', $url )."' async='async";
		}
	}
	add_filter( 'clean_url', 'cb_async_scripts', 11, 1 );


	function cb_defer_scripts( $url ) {
		if ( strpos( $url, '#deferload' ) === false ) {
			return $url;
		} elseif ( is_admin() ) {
			return str_replace( '#deferload', '', $url );
		} else {
			return str_replace( '#deferload', '', $url )."' defer='defer";
		}
	}
	add_filter( 'clean_url', 'cb_defer_scripts', 11, 1 );


	function cb_assets() {

		$theme_dir      = get_template_directory() . '/assets/css/style.min.css';
		$theme_filetime = filemtime( $theme_dir );
		$theme          = get_template_directory_uri() . '/assets/css/style.min.css?v=' . $theme_filetime;
		wp_enqueue_style( 'theme', $theme, null, 1.0 );

		if ( ! is_admin() && ! is_user_logged_in() ) {
			wp_deregister_script( 'jquery' );
			wp_deregister_script( 'wp-embed' );
		}

		wp_enqueue_script( 'jquery' );

		// $vendors_dir = get_template_directory() . '/assets/js/vendors.js';
		// $vendors_filetime = filemtime( $vendors_dir );
		// $vendors = get_template_directory_uri() . '/assets/js/vendors.js#deferloadv?v=' . $vendors_filetime;
		// wp_enqueue_script( 'vendors', $vendors, null, 1.0, true );

		// $custom_dir = get_template_directory() . '/assets/js/custom.js';
		// $custom_filetime = filemtime( $custom_dir );
		// $custom = get_template_directory_uri() . '/assets/js/custom.js?v=' . $custom_filetime;
		// wp_enqueue_script( 'custom', $custom, null, 1.0, true );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			// wp_enqueue_script( 'comment-reply' );
		}
	}
	add_action( 'wp_enqueue_scripts', 'cb_assets' );

endif;

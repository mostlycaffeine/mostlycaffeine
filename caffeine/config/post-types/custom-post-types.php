<?php
/**
 * Custom Post Types
 *
 * @link https://generatewp.com/post-type/
 *
 * @package caffeinebuilt
 */


function portfolio() {

	$labels = array(
		'name'                  => _x( 'Portfolio', 'Post Type General Name', 'caffeinebuilt' ),
		'singular_name'         => _x( 'Portfolio item', 'Post Type Singular Name', 'caffeinebuilt' ),
		'menu_name'             => __( 'Portfolio', 'caffeinebuilt' ),
		'name_admin_bar'        => __( 'Portfolio', 'caffeinebuilt' ),
		'archives'              => __( 'Item Archives', 'caffeinebuilt' ),
		'attributes'            => __( 'Item Attributes', 'caffeinebuilt' ),
		'parent_item_colon'     => __( 'Parent Item:', 'caffeinebuilt' ),
		'all_items'             => __( 'All Items', 'caffeinebuilt' ),
		'add_new_item'          => __( 'Add New Item', 'caffeinebuilt' ),
		'add_new'               => __( 'Add New', 'caffeinebuilt' ),
		'new_item'              => __( 'New Item', 'caffeinebuilt' ),
		'edit_item'             => __( 'Edit Item', 'caffeinebuilt' ),
		'update_item'           => __( 'Update Item', 'caffeinebuilt' ),
		'view_item'             => __( 'View Item', 'caffeinebuilt' ),
		'view_items'            => __( 'View Items', 'caffeinebuilt' ),
		'search_items'          => __( 'Search Item', 'caffeinebuilt' ),
		'not_found'             => __( 'Not found', 'caffeinebuilt' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'caffeinebuilt' ),
		'featured_image'        => __( 'Featured Image', 'caffeinebuilt' ),
		'set_featured_image'    => __( 'Set featured image', 'caffeinebuilt' ),
		'remove_featured_image' => __( 'Remove featured image', 'caffeinebuilt' ),
		'use_featured_image'    => __( 'Use as featured image', 'caffeinebuilt' ),
		'insert_into_item'      => __( 'Insert into item', 'caffeinebuilt' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'caffeinebuilt' ),
		'items_list'            => __( 'Items list', 'caffeinebuilt' ),
		'items_list_navigation' => __( 'Items list navigation', 'caffeinebuilt' ),
		'filter_items_list'     => __( 'Filter items list', 'caffeinebuilt' ),
	);
	$args = array(
		'label'                 => __( 'Post Type', 'caffeinebuilt' ),
		'description'           => __( 'Post Type Description', 'caffeinebuilt' ),
		'labels'                => $labels,
		'supports'              => array( 'title', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'portfolio', $args );

}
add_action( 'init', 'portfolio', 0 );

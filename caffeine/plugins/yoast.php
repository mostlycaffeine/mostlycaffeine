<?php
/**
 * Yoast.
 * https://yoast.com/
 *
 * @package caffeinebuilt
 */

/** Remove Yoast dashboard widget */
function cb_remove_yoast_dashboard_widgets() {
	remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'normal' );
}
add_action( 'admin_init', 'cb_remove_yoast_dashboard_widgets' );


/** Remove Yoast json search from head */
add_filter( 'disable_wpseo_json_ld_search', '__return_true' );


/** Remove Yoast admin bar links */
function cb_remove_yoast_admin_bar() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu( 'wpseo-menu' );
	$wp_admin_bar->remove_menu( 'comments' );
}
add_action( 'wp_before_admin_bar_render', 'cb_remove_yoast_admin_bar' );


/** Move Yoast to bottom */
function cb_move_yoast_lower() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'cb_move_yoast_lower' );


/** Remove Yoast from certain CPT's **/
// function cb_remove_yoast_metabox_cpts() {
// 	// remove_meta_box( 'wpseo_meta', 'portfolio', 'normal' );
// }
// add_action( 'add_meta_boxes', 'cb_remove_yoast_metabox_cpts', 11 );

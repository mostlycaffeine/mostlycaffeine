<?php
/**
 * Advanced Custom Fields related functions
 *
 * @package caffeinebuilt
 */

/* https://www.advancedcustomfields.com/resources/local-json/ */
function cb_acf_json_save_point( $path ) {
	$path = get_stylesheet_directory() . '/caffeine/acf-json';
	return $path;
}
add_filter( 'acf/settings/save_json', 'cb_acf_json_save_point' );

function cb_acf_json_load_point( $paths ) {
	unset( $paths[0] );
	$paths[] = get_stylesheet_directory() . '/caffeine/acf-json';
	return $paths;
}
add_filter( 'acf/settings/load_json', 'cb_acf_json_load_point' );



/* Advanced Custom Fields options pages and sub pages. */
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( array(
		'page_title'  => 'Website Options',
		'menu_title'  => 'Website Options',
		'menu_slug'   => 'website-options',
		'capability'  => 'edit_posts',
		'redirect'    => true,
	));
	acf_add_options_sub_page( array(
		'page_title'  => 'Global',
		'menu_title'  => 'Global',
		'parent_slug' => 'website-options',
	));
}



/**
 * ACF Gutenberg Blocks.
 *
 * @link https://wordpress.org/gutenberg/handbook/
 * @link https://www.advancedcustomfields.com/resources/acf_register_block/
 */
function cb_acf_init() {
	if ( function_exists( 'acf_register_block' ) ) {
		acf_register_block(array(
			'name'            => 'twocolcontent',
			'title'           => __( 'Two column content' ),
			'description'     => __( 'A custom 2 column content block.' ),
			'render_callback' => 'cb_acf_block_render_callback',
			'category'        => 'formatting',
			'icon'            => 'admin-comments',
			'keywords'        => array( '2col', '2 col' ),
		));
	}
}
add_action( 'acf/init', 'cb_acf_init' );


/**
 * This is the callback that displays the testimonial block.
 *
 * @param  array $block The block settings and attributes.
 */
function cb_acf_block_render_callback( $block ) {

	// Convert block to nice template name.
	$slug = str_replace( 'acf/', '', $block['name'] );

	// Include a template part from within the "template-parts/block" folder.
	if ( file_exists( STYLESHEETPATH . "/template-parts/block/content-{$slug}.php" ) ) {
		include( STYLESHEETPATH . "/template-parts/block/content-{$slug}.php" );
	}
}

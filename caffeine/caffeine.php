<?php
/**
 * Caffeine functions and definitions
 *
 * @package caffeinebuilt
 */

/****************************************
 * Includes.
 * **************************************
 */

/**
 * Security.
 */
require get_template_directory() . '/caffeine/inc/security.php';
/**
 * Cleanup.
 */
require get_template_directory() . '/caffeine/inc/cleanup.php';
/**
 * Gutenberg.
 */
require get_template_directory() . '/caffeine/inc/gutenberg.php';
/**
 * Theme wrapper.
 */
require get_template_directory() . '/caffeine/inc/wrapper.php';
/**
 * Allowed mime types.
 */
require get_template_directory() . '/caffeine/inc/mime-types.php';
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/caffeine/inc/template-tags.php';
/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/caffeine/inc/template-functions.php';
/**
 * Customizer additions.
 */
require get_template_directory() . '/caffeine/inc/customizer.php';
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/caffeine/inc/custom-header.php';





/****************************************
 * Plugins.
 * **************************************
 */

/**
 * Load Advanced Custom Fields compatibility file.
 */
if ( class_exists( 'acf' ) ) {
	require get_template_directory() . '/caffeine/plugins/acf.php';
}
/**
 * Load Yoast compatibility file.
 */
if ( defined( 'WPSEO_VERSION' ) ) {
	require get_template_directory() . '/caffeine/plugins/yoast.php';
}
/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/caffeine/plugins/jetpack.php';
}
/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/caffeine/plugins/woocommerce.php';
}





/****************************************
 * Functions.
 * **************************************
 */

/**
 * Remove users from RestAPI.
 */
require get_template_directory() . '/caffeine/functions/restapi-remove-users.php';
/**
 * Remove fields from RestAPI.
 */
require get_template_directory() . '/caffeine/functions/restapi-remove-fields.php';
/**
 * Add category list with URL's to RestAPI.
 */
require get_template_directory() . '/caffeine/functions/restapi-add-category-links.php';
/**
 * Change menu items with anchor links to buttons.
 */
require get_template_directory() . '/caffeine/functions/menu-hash-links-to-buttons.php';
/**
 * Add slug to menu class.
 */
require get_template_directory() . '/caffeine/functions/menu-add-slug-as-class.php';
/**
 * Gutenberg.
 */
require get_template_directory() . '/caffeine/functions/gutenberg.php';
/**
 * Pagination offset.
 */
// require get_template_directory() . '/caffeine/functions/pagination-offset.php';





/****************************************
 * Helpers.
 * **************************************
 */

/**
 * Video converter.
 */
// require get_template_directory() . '/caffeine/helpers/video-converter.php';





/****************************************
 * Config.
 * **************************************
 */

/**
 * Assets (CSS / JS).
 */
require get_template_directory() . '/caffeine/config/assets.php';
/**
 * Images.
 */
require get_template_directory() . '/caffeine/config/images.php';
/**
 * Files.
 */
require get_template_directory() . '/caffeine/config/files.php';
/**
 * Sidebars.
 */
require get_template_directory() . '/caffeine/config/sidebars.php';
/**
 * Menus.
 */
require get_template_directory() . '/caffeine/config/menus.php';
/**
 * Post types.
 */
require get_template_directory() . '/caffeine/config/post-types/custom-post-types.php';





/****************************************
 * Vendors.
 * **************************************
 */

/**
 * Include composer autoload if used.
 */
$cb_composer = get_template_directory() . '/caffeine/vendor/autoload.php';
if ( file_exists( $cb_composer ) ) {
	require $cb_composer;
}
/**
 * WhichBrowser
 * https://github.com/WhichBrowser/Parser-PHP
 */
// if ( class_exists( 'WhichBrowser\Parser' ) ) {
// 	$wb_result = new WhichBrowser\Parser( getallheaders() );
// }

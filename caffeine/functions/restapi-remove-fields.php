<?php
/**
 * Remove fields from RestAPI.
 *
 * @package caffeinebuilt
 */
function cb_remove_fields_restapi( $data, $post, $request ) {
	$_data = $data->data;

	unset( $data->data ['author'] );
	// unset( $data->data ['tags'] );
	// unset( $data->data ['content'] );
	// unset( $data->data ['featured_media'] );
	// unset( $data->data ['comment_status'] );
	// unset( $data->data ['ping_status'] );
	// unset( $data->data ['sticky'] );
	// unset( $data->data ['template'] );
	// unset( $data->data ['format'] );
	// unset( $data->data ['template'] );
	// unset( $data->data ['categories'] );
	// unset( $data->data ['guid'] );
	// unset( $data->data ['meta'] );

	return $data;
}

add_filter( 'rest_prepare_post', 'cb_remove_fields_restapi', 10, 3 );

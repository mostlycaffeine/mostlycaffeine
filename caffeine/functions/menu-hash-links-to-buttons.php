<?php
/**
 * Menu hash links to buttons.
 *
 * @package caffeinebuilt
 */

/**
 * Remove the href from empty links `<a href="#">` in the nav menus
 *
 * @param string $menu the current menu HTML.
 * @return string the modified menu HTML.
 */
function cb_remove_empty_links( $menu ) {
	return preg_replace( '/<a href=\\"#([\\s\\S]*?)">([\\s\\S]*?)<\/a>/', '<button class="menu-button" data-hash-location="\\1">\\2</button>', $menu );
}

add_filter( 'wp_nav_menu_items', 'cb_remove_empty_links' );

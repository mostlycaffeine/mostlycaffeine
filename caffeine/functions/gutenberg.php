<?php
/**
 * Gutenberg related functions.
 *
 * @package caffeinebuilt
 */

/**
 * Change autosave time to fix never ending save bug.
 */
add_filter( 'block_editor_settings', 'cb_block_editor_settings', 10, 2 );

function cb_block_editor_settings( $editor_settings, $post ) {
	$editor_settings['autosaveInterval'] = 2000; //number of second [default value is 10]

	return $editor_settings;
}

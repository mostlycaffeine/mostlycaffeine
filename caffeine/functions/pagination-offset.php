<?php
/**
 * Pagination offset.
 *
 * Offsets the pagination.
 *
 * @package caffeinebuilt
 *
 * @param string $query Main WordPress query.
 */
function cb_query_offset( $query ) {
	if ( ! ( is_front_page() ) || ! ( $query->is_home() || $query->is_main_query() ) ) {
		return;
	}

	if ( is_admin() || $query->is_category() || $query->is_search() ) {
		return;
	}

	$offset = 1; // Change this to whatever needs to be offset.
	$ppp = get_option( 'posts_per_page' );

	if ( $query->is_paged && $query->is_home() && $query->is_main_query() ) {

		$page_offset = $offset + ( ( $query->query_vars['paged']-1 ) * $ppp );
		$query->set( 'offset', $page_offset );

	} elseif ( ! $query->is_paged && $query->is_home() && $query->is_main_query() ) {

		$query->set( 'posts_per_page', $offset + $ppp );

	}
}

add_action( 'pre_get_posts', 'cb_query_offset', 1 );



/**
 * Pagination offset adjust.
 *
 * Adjust the found_posts according to our offset.
 *
 * @param string $found_posts Number of found posts for the query.
 * @param string $query Main WordPress query.
 */
function cb_adjust_offset_pagination( $found_posts, $query ) {
	if ( ! $query->is_main_query() ) {
		return $found_posts;
	}
	if ( ! $query->is_home() || $query->is_category() || is_admin() || $query->is_archive() || $query->is_search() ) {
		return $found_posts;
	}

	$offset = 1; // Change this to whatever needs to be offset.

	if ( $query->is_home() && is_main_query() && ! is_admin() && ! $query->is_category() && ! $query->is_archive() && ! $query->is_search() ) {
		return $found_posts - $offset;
	}

	return $found_posts;
}

add_filter( 'found_posts', 'cb_adjust_offset_pagination', 1, 2 );

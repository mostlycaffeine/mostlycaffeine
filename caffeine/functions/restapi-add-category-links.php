<?php
/**
 * Add category links to RestAPI.
 *
 * Adding categories as a simple list with links.
 *
 * @package caffeinebuilt
 */
function cb_add_category_links_restapi( $data, $post, $request ) {
	$_data = $data->data;

	$terms = get_the_terms( $post->ID, 'category' );
	$category_names = '';
	$category_links = '';
	if ( is_array( $terms ) || is_object( $terms ) ) {
		foreach ( $terms as $term ) {
			$category_names .= $term->name . ', ';
			$category_links .= '<a href="' . get_term_link( $term ) . '">' . $term->name . '</a>, ';
		}
	}

	$_data['category_list'] = substr( $category_names, 0, -2 );
	$_data['category_list_links'] = substr( $category_links, 0, -2 );

	$_data['post_summary'] = get_post_meta( $post->ID, 'post_summary', true );

	$data->data = $_data;

	return $data;
}

add_filter( 'rest_prepare_post', 'cb_add_category_links_restapi', 10, 3 );

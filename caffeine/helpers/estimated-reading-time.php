<?php
/**
 * The estimated time required to read a post.
 *
 * @package caffeinebuilt
 *
 * To do: Add saving the time to posts meta field.
 *
 * Usage: <?php echo esc_attr( cb_estimated_reading_time() ); ?>
 */
function cb_estimated_reading_time() {

	$post = get_post();

	$words = str_word_count( strip_tags( $post->post_content ) );
	$mins = floor( $words / 120 );

	if ( 1 <= $mins ) {
		$estimated_time = $mins . ' min read';
	} else {
		$estimated_time = '1 min read';
	}

	return $estimated_time;

}

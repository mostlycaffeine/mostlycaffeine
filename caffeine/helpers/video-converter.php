<?php
/**
 * Video Converter.
 *
 * @package caffeinebuilt
 *
 * The idea behind this is that if a Video Link is pasted into a meta field
 * then this converts it to a responsive iframe.
 *
 * Supports YouTube & Vimeo.
 *
 * @param string $video_code The URL being passed, likely from a meta field.
 * @param string $video_title The video title for accessibility.
 * @param string $video_class Unique class for wrapper.
 *
 * Usage: cb_responsive_video( 'https://www.youtube.com/watch?v=WWum0VRc6MI', '2Spooky4You', 'spooky' );.
 */
function cb_responsive_video( $video_code, $video_title, $video_class ) {
	if ( strpos( $video_code, 'youtube' ) !== false ) {
		$video_code = str_replace( 'https://www.youtube.com/watch?v=', '', $video_code );
		$video_code = str_replace( 'http://youtube.com/watch?v=', '', $video_code );
		$video_code = str_replace( 'https://www.youtube.com/embed/', '', $video_code );
		$video_code = str_replace( 'http://www.youtube.com/embed/', '', $video_code );

		echo '
			<div class="embed-container youtube-video lazyload--video ' . esc_attr( $video_class ) . '">
				<iframe data-src="https://www.youtube-nocookie.com/embed/' . esc_attr( $video_code ) . '?modestbranding=0&autoplay=0&rel=0&showinfo=0" gesture="media" allow="encrypted-media" allowfullscreen title="' . esc_attr( $video_title ) . '" class="lazyload"></iframe>
				<noscript><a href="https://www.youtube-nocookie.com/embed/' . esc_attr( $video_code ) . '" target="_blank" rel="noreferrer nofollow noopener" style="display: block; margin-top: 45px; position:relative; text-align: center; z-index:5;">You do not have JavaScript enabled, please click here to view this video on YouTube.</a></noscript>
			</div>
		';

	} elseif ( strpos( $video_code, 'vimeo' ) !== false ) {
		$video_code = str_replace( 'https://vimeo.com/', '', $video_code );
		$video_code = str_replace( 'http://vimeo.com/', '', $video_code );
		$video_code = str_replace( 'https://www.vimeo.com/', '', $video_code );
		$video_code = str_replace( 'http://www.vimeo.com/', '', $video_code );

		echo '
			<div class="embed-container vimeo-video lazyload--video ' . esc_attr( $video_class ) . '">
				<iframe data-src="https://player.vimeo.com/video/' . esc_attr( $video_code ) . '?title=0&byline=0&portrait=0" allowfullscreen title="' . esc_attr( $video_title ) . '" class="lazyload"></iframe>
				<noscript><a href="https://player.vimeo.com/video/' . esc_attr( $video_code ) . '" target="_blank" rel="noreferrer nofollow noopener" style="position:relative; z-index:5;">Click here for ' . esc_attr( $video_title ) . ' video</a></noscript>
			</div>
		';

	}
}


function cb_video_image_url( $video_code ) {
	if ( strpos( $video_code, 'youtube' ) !== false ) {
		$video_code = str_replace( 'https://www.youtube.com/watch?v=', '', $video_code );
		$video_code = str_replace( 'http://youtube.com/watch?v=', '', $video_code );
		$video_code = str_replace( 'https://www.youtube.com/embed/', '', $video_code );
		$video_code = str_replace( 'http://www.youtube.com/embed/', '', $video_code );

		echo 'https://img.youtube.com/vi/' . esc_attr( $video_code );
	} else {
		echo '';
	}
}

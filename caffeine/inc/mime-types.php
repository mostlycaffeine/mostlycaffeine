<?php
/**
 * Supported mime types.
 *
 * Default allowed list can be found here: https://codex.wordpress.org/Function_Reference/get_allowed_mime_types
 *
 * @package caffeinebuilt
 */

/**
 * Add or unset (remove) allowed mime types and file extensions.
 *
 * @param array $mimes Current array of mime types.
 */
function cb_supported_mime_types( $mimes ) {
	$mimes['doc'] = 'application/msword';
	$mimes['svg'] = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';

	unset( $mimes['exe'] );

	return $mimes;
}

add_filter( 'upload_mimes', 'cb_supported_mime_types' );

<?php
/**
 * Cleanup WordPress.
 *
 * @package caffeinebuilt
 */

/* Remove head junk. */
function cb_head_cleanup() {
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'index_rel_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	add_filter( 'feed_links_show_comments_feed', '__return_false' );
	add_filter( 'the_generator', '__return_false' );
}
add_action( 'init', 'cb_head_cleanup' );

function cb_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'cb_remove_recent_comments_style' );



/* Remove dashboard widgets */
function cb_remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments'] );
	// unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'] );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
	remove_action( 'welcome_panel', 'wp_welcome_panel' );
}
add_action( 'wp_dashboard_setup', 'cb_remove_dashboard_widgets' );



/* Replace WP logo. */
function cb_custom_login_logo() {
	echo '<style type="text/css">
        h1 {
            background-image: none !important;
            padding: 0 !important;
            margin: 0 !important;
            display: none !important;
        }
    </style>';
}
add_action( 'login_head', 'cb_custom_login_logo' );



/* Set the Admin footer. */
function cb_remove_footer_admin() {
	echo 'Powered by <a href="http://www.wordpress.org" target="_blank">WordPress</a> &amp; fueled by <a href="https://mostlycaffeine.com" target="_blank">@mostlycaffeine</a>.</p>';
}
add_filter( 'admin_footer_text', 'cb_remove_footer_admin' );

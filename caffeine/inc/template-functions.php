<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package caffeinebuilt
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function cb_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'cb_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function cb_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'cb_pingback_header' );

/**
 * Filter the "read more" excerpt string link to the post.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function cb_excerpt_more( $more ) {

	if ( ! is_single() ) {
		$more = sprintf( '... <a class="read-more" href="%1$s">%2$s</a>', get_permalink( get_the_ID() ), __( 'Read more', 'caffeinebuilt' ) );
	}

	return $more;
}
add_filter( 'excerpt_more', 'cb_excerpt_more' );

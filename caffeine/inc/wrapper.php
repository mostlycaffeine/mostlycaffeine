<?php
/**
 * Uses base.php as a theme wrapper
 *
 * @link http://scribu.net/wordpress/theme-wrappers.html
 *
 * @package caffeinebuilt
 */

if ( ! function_exists( 'cb_template_path' ) ) :

	function cb_template_path() {
		return CB_Wrapping::$main_template;
	}

	class CB_Wrapping {
		public static $main_template;
		public $slug;
		public $templates;
		static $base;

		public function __construct( $template = 'base.php' ) {
			$this->slug = basename( $template, '.php' );
			$this->templates = array( $template );
			if ( self::$base ) {
				$str = substr( $template, 0, -4 );
					array_unshift( $this->templates, sprintf( $str . '-%s.php', self::$base ) );
			}
		}

		public function __toString() {
			$this->templates = apply_filters( 'caffeine/wrap_' . $this->slug, $this->templates );
			return locate_template( $this->templates );
		}

		static function wrap( $main ) {
			if ( ! is_string( $main ) ) {
				return $main;
			}
			self::$main_template = $main;
			self::$base = basename( self::$main_template, '.php' );
			if ( 'index' === self::$base ) {
				self::$base = false;
			}
			return new CB_Wrapping();
		}
	}
	add_filter( 'template_include', array( 'CB_Wrapping', 'wrap' ), 99 );

endif;

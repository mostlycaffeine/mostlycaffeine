<?php
/**
 * Yoast.
 * https://yoast.com/
 *
 * @package caffeinebuilt
 */

/* Disable ping back scanner and complete xmlrpc class. */
add_filter( 'wp_xmlrpc_server_class', '__return_false' );
add_filter( 'xmlrpc_enabled', '__return_false' );


/* Block WP enum scans for security against Brute Force attacks. */
if ( ! is_admin() ) {
	if ( preg_match( '/author=([0-9]*)/i', $_SERVER['QUERY_STRING'] ) ) {
		die();
	}
	add_filter( 'redirect_canonical', 'cb_check_enum', 10, 2 );
}
function cb_check_enum( $redirect, $request ) {
	if ( preg_match( '/\?author=([0-9]*)(\/*)/i', $request ) ) {
		die();
	} else {
		return $redirect;
	}
}


/* Remove the Admin login errors for extra security. */
function cb_no_wordpress_errors() {
	return 'Please try again.';
}
add_filter( 'login_errors', 'cb_no_wordpress_errors' );


/* Remove Meta Poweredby widget. */
add_filter( 'widget_meta_poweredby', '__return_empty_string' );


/** Remove WordPress version param from any enqueued scripts **/
function cb_remove_wp_version_from_assets( $src ) {
	if ( strpos( $src, 'ver=' ) ) {
		$src = remove_query_arg( 'ver', $src );
		return $src;
	}
}
add_filter( 'style_loader_src', 'cb_remove_wp_version_from_assets', 9999 );
add_filter( 'script_loader_src', 'cb_remove_wp_version_from_assets', 9999 );



/**
 * Force URLs in srcset attributes into HTTPS scheme.
 * This is particularly useful when you're running a Flexible SSL frontend like Cloudflare
 * https://wordpress.org/support/topic/responsive-images-src-url-is-https-srcset-url-is-http-no-images-loaded/#post-6829020
 */
function cb_ssl_srcset( $sources ) {
	foreach ( $sources as &$source ) {
		$source['url'] = set_url_scheme( $source['url'], '//' );
	}
	return $sources;
}
add_filter( 'wp_calculate_image_srcset', 'cb_ssl_srcset' );



/**
 * Redirect category and tag pages.
 */
function cb_category_tag_redirect() {
	if ( is_category() || is_tag() ) {
		$url = site_url( '/blog' );
		wp_safe_redirect( $url, 302 );
		exit();
	}
}
add_action( 'template_redirect', 'cb_category_tag_redirect' );

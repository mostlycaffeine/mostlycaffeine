<?php
/**
 * The clients section.
 *
 * @package caffeinebuilt
 */

$clients_heading       = get_field( 'clients_heading' );
$clients_content       = get_field( 'clients_content' );
$clients_content_clean = apply_filters( 'meta_content', $clients_content );

if ( $clients_heading || $clients_content ) {
?>
<section class="section__clients">
	<div class="section__inner medium-text">
		<div class="section__inner--left">
			<?php
			if ( $clients_heading ) {
				echo '<h2><span>' . esc_attr( $clients_heading ) . '</span></h2>';
			}
			?>
		</div>

		<div class="section__inner--right">
			<?php
			if ( $clients_content ) {
				echo wp_kses_post( $clients_content_clean );
			}
			?>
		</div>
	</div>
</section>
<?php
}

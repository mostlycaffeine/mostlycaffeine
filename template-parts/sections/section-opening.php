<?php
/**
 * The opening section.
 *
 * @package caffeinebuilt
 */

$opening_heading   = get_field( 'opening_heading' );
$opening_strapline = get_field( 'opening_strapline' );
?>

<header class="section__opening">
	<div class="section__opening--inner">

		<div class="heading-logo">
			<svg class="svg-inline--fa fa-coffee" aria-hidden="true" data-fa-i2svg="" data-prefix="fas" data-icon="coffee" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
				<desc><?php esc_html_e( 'mostlycaffeine.com logo of coffee cup', 'caffeinebuilt' ); ?></desc>
				<path fill="currentColor" d="M192 384h192c53 0 96-43 96-96h32c70.6 0 128-57.4 128-128S582.6 32 512 32H120c-13.3 0-24 10.7-24 24v232c0 53 43 96 96 96zM512 96c35.3 0 64 28.7 64 64s-28.7 64-64 64h-32V96h32zm47.7 384H48.3c-47.6 0-61-64-36-64h583.3c25 0 11.8 64-35.9 64z"></path>
			</svg>
		</div>

		<div class="heading-content">
			<?php
			if ( $opening_heading ) {
			?>
			<h1>
				<span class="heading-bg">
					<?php echo esc_attr( $opening_heading ); ?>
				</span>
				<span class="heading-blurb">
					<?php echo esc_attr( $opening_strapline ); ?>
				</span>
			</h1>
			<?php
			}
			?>
		</div>
	</div>
</header>

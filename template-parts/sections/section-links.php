<?php
/**
 * The links section.
 *
 * @package caffeinebuilt
 */

$email_address = get_field( 'email_address', 'options' );
$email_address = filter_var( $email_address, FILTER_SANITIZE_EMAIL );

$linkedin_url  = get_field( 'linkedin_url', 'options' );
$twitter_url   = get_field( 'twitter_url', 'options' );
$gitlab_url    = get_field( 'gitlab_url', 'options' );
$slack_url     = get_field( 'slack_url', 'options' );
?>

<footer id="footer">
	<?php
	if ( ! empty( $email_address ) ) {
	?>
		<p class="footer__email">
			<?php esc_html_e( 'Email', 'caffeinebuilt' ); ?>: <a href="mailto:<?php echo sanitize_email( $email_address ); ?>" target="_blank" rel="noopener noreferrer" title="<?php esc_html_e( 'Email me', 'caffeinebuilt' ); ?>"><?php echo sanitize_email( $email_address ); ?></a>
		</p>
	<?php
	}
	?>

	<p>
		<?php
		if ( ! empty( $twitter_url ) ) {
		?>
			<a href="<?php echo esc_url( $twitter_url ); ?>" target="_blank" rel="noopener noreferrer" title="<?php esc_html_e( 'Follow me on Twitter', 'caffeinebuilt' ); ?>"><?php esc_html_e( 'Twitter', 'caffeinebuilt' ); ?></a>
			<span class="footer-spacer"> / </span>
		<?php
		}
		?>
		<?php
		if ( ! empty( $linkedin_url ) ) {
		?>
			<a href="<?php echo esc_url( $linkedin_url ); ?>" target="_blank" rel="noopener noreferrer" title="<?php esc_html_e( 'Add me on LinkedIn', 'caffeinebuilt' ); ?>"><?php esc_html_e( 'LinkedIn', 'caffeinebuilt' ); ?></a>
			<span class="footer-spacer"> / </span>
		<?php
		}
		?>
		<?php
		if ( ! empty( $gitlab_url ) ) {
		?>
			<a href="<?php echo esc_url( $gitlab_url ); ?>" target="_blank" rel="noopener noreferrer" title="<?php esc_html_e( 'Follow me on GitLab', 'caffeinebuilt' ); ?>"><?php esc_html_e( 'GitLab', 'caffeinebuilt' ); ?></a>
		<?php
		}
		?>
	</p>

	<?php
	if ( ! empty( $slack_url ) ) {
	?>
		<p class="footer__tiny-text">
			<?php esc_html_e( 'You can also find me on', 'caffeinebuilt' ); ?> <a href="<?php echo esc_url( $slack_url ); ?>" target="_blank" rel="noopener noreferrer nofollow" title="<?php esc_html_e( 'WordPress UK Slack', 'caffeinebuilt' ); ?>"><?php esc_html_e( 'WordPress UK Slack', 'caffeinebuilt' ); ?></a>
		</p>
	<?php
	}
	?>

</footer>

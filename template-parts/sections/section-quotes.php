<?php
/**
 * The quotes section.
 *
 * @package caffeinebuilt
 */

$quote_heading    = get_field( 'quote_heading' );
$quote_leftquote  = get_field( 'quote_leftquote' );
$quote_rightquote = get_field( 'quote_rightquote' );

if ( $quote_leftquote && $quote_rightquote ) {
?>
<section class="section__quotes">
	<div class="section__inner medium-text">
		<div class="heading">
		<?php
		if ( $quote_heading ) {
			echo '<h2><span>' . esc_attr( $quote_heading ) . '</span></h2>';
		}
		?>
		</div>
		<div class="section__inner--left">
			<?php
			if ( $quote_leftquote ) {
				echo '<blockquote>' . esc_attr( $quote_leftquote ) . '</blockquote>';
			}
			?>
		</div>
		<div class="section__inner--right">
			<?php
			if ( $quote_rightquote ) {
				echo '<blockquote>' . esc_attr( $quote_rightquote ) . '</blockquote>';
			}
			?>
		</div>
	</div>
</section>
<?php
}

<?php
/**
 * The about section.
 *
 * @package caffeinebuilt
 */

$about_content       = get_field( 'about_content' );
$about_content_clean = apply_filters( 'meta_content', $about_content );

if ( $about_content ) {
?>
<section class="section__about">
	<div class="section__inner large-text">
		<?php echo wp_kses_post( $about_content ); ?>

		<p><a href="<?php echo esc_url( site_url() ); ?>/services/" title="<?php esc_html_e( 'View my services', 'caffeinebuilt' ); ?>" class="button"><?php esc_html_e( 'View my services', 'caffeinebuilt' ); ?></a></p>
	</div>
</section>
<?php
}

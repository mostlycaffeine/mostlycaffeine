<?php
/**
 * The skills section.
 *
 * @package caffeinebuilt
 */

$skills_heading = get_field( 'skills_heading' );
$skills_text    = get_field( 'skills_text' );
?>

<section class="section__skills">
	<div class="section__inner medium-text">
		<div class="section__inner--left">
			<?php
			if ( $skills_heading ) {
				echo '<h2><span>' . esc_attr( $skills_heading ) . '</span></h2>';
			}
			?>
		</div>

		<div class="section__inner--right">

			<p>
				<?php echo esc_attr( $skills_text ); ?>
			</p>

			<hr />

			<ul>
				<?php
				$skills_overview = get_field( 'skills_overview' );

				if ( $skills_overview ) {
					foreach ( $skills_overview as $skills_overview_item ) {
				?>

					<li>
						<?php echo esc_attr( $skills_overview_item['item'] ); ?>
					</li>

				<?php
					}
				} else {
				?>

					<?php esc_html_e( 'No items added, yet...', 'caffeinebuilt' ); ?>

				<?php
				}
				?>
			</ul>

			<hr />

			<ul>
				<?php
				$skills_list = get_field( 'skills_list' );

				if ( $skills_list ) {
					foreach ( $skills_list as $skills_list_item ) {
				?>

					<li>
						<?php echo esc_attr( $skills_list_item['item'] ); ?>
					</li>

				<?php
					}
				} else {
				?>

					<?php esc_html_e( 'No items added, yet...', 'caffeinebuilt' ); ?>

				<?php
				}
				?>
			</ul>

		</div>
	</div>
</section>

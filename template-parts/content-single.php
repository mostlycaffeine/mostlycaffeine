<?php
/**
 * Template part for displaying posts
 *
 * @package caffeinebuilt
 */

?>

<article id="post-<?php the_ID(); ?>" class="<?php if ( !is_single() ) : echo "col-xs-12 col-sm-12 col-md-9 col-lg-9"; endif; ?>">
	<div <?php post_class(); ?>>
		<header class="section__opening page__opening">
			<div class="section__opening--inner">
				<h1>
					<span class="heading-bg">
					<?php
						if ( is_single() ) :
							the_title();
						// else :
						// 	the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						endif;
					?>
					</span>
				</h1>

				<?php
					if ( 'post' === get_post_type() ) :
				?>
					<div class="entry-meta">
						<?php cb_posted_on(); ?>

						<div class="categories-list">
							<ul>
								<?php the_category( ' ' ); ?>
							</ul>
						</div>
					</div>
				<?php
					endif;
				?>
			</div>
		</header>
		<div class="entry-content medium-text">
			<?php
				the_content( sprintf(
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'caffeinebuilt' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'caffeinebuilt' ),
					'after'  => '</div>',
				) );
			?>
		</div>
	</div>
</article>

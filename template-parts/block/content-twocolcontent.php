<?php
/**
 * Two column content.
 * A gutenberg block.
 *
 * @link https://wordpress.org/gutenberg/handbook/
 *
 * @package caffeinebuilt
 */

$two_col_heading  = get_field( '2col_heading' );
$two_col_textarea = get_field( '2col_textarea' );
?>

	<section class="section__white">
		<div class="section__inner medium-text">
			<div class="section__inner--left">
				<?php if ( ! empty( $two_col_heading ) ) { ?>
					<h2><span><?php echo esc_attr( $two_col_heading ); ?></span></h2>
				<?php } ?>
			</div>

			<div class="section__inner--right">
				<?php if ( $two_col_textarea ) { ?>
					<p><?php echo wp_kses_post( $two_col_textarea ); ?></p>
				<?php } ?>
			</div>
		</div>
	</section>

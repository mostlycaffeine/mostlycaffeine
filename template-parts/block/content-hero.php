<?php
/**
 * Block Name: Hero
 *
 * This is the template that displays the hero block.
 */

$hero = get_field( 'hero' );
?>

<h1>
<?php echo esc_attr( $hero ); ?>
</h1>

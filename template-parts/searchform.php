<?php
/**
 * The template for displaying the search form
 *
 * @package caffeinebuilt
 */

?>
<form role="search" method="get" class="search-form form-inline" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label class="screen-reader-text"><?php esc_html_e( 'Search for', 'caffeinebuilt' ); ?>:</label>
	<div class="input-group">
		<input type="search" value="<?php echo get_search_query(); ?>" name="s" class="search-field form-control" placeholder="<?php esc_html_e( 'Search', 'caffeinebuilt' ); ?>" required>
		<span class="input-group-btn">
			<button type="submit" class="search-submit"><?php esc_html_e( 'Search', 'caffeinebuilt' ); ?></button>
		</span>
	</div>
</form>

<?php
/**
 * Footer
 *
 * @package caffeinebuilt
 */

?>

		<?php get_template_part( 'template-parts/sections/section-links' ); ?>

		<?php wp_footer(); ?>

	</body>
</html>

<?php
/**
 * The global header.
 *
 * @package caffeinebuilt
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'caffeinebuilt' ); ?></a>

		<nav>
			<div class="menu__inner">
				<div class="menu__left">
					<ul>
						<li>
							<a class="coffeemug" href="<?php echo esc_url( site_url() ); ?>" title="<?php echo esc_attr_e( 'Home page', 'caffeinebuilt' ); ?>">
								<svg class="svg-inline--fa fa-coffee" aria-hidden="true" data-fa-i2svg="" data-prefix="fas" data-icon="coffee" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
									<desc><?php esc_html_e( 'mostlycaffeine.com logo of coffee cup', 'caffeinebuilt' ); ?></desc>
									<path fill="currentColor" d="M192 384h192c53 0 96-43 96-96h32c70.6 0 128-57.4 128-128S582.6 32 512 32H120c-13.3 0-24 10.7-24 24v232c0 53 43 96 96 96zM512 96c35.3 0 64 28.7 64 64s-28.7 64-64 64h-32V96h32zm47.7 384H48.3c-47.6 0-61-64-36-64h583.3c25 0 11.8 64-35.9 64z"></path>
								</svg>
							</a>
						</li>
					</ul>
				</div>
				<div class="menu__right">
					<ul>
					<li>
							<a href="<?php echo esc_url( site_url() ); ?>/services/" title="<?php echo esc_attr_e( 'Services page', 'caffeinebuilt' ); ?>"><?php echo esc_attr_e( 'Services', 'caffeinebuilt' ); ?></a>
						</li>
						<li>
							<a href="<?php echo esc_url( site_url() ); ?>/blog/" title="<?php echo esc_attr_e( 'Blog page', 'caffeinebuilt' ); ?>"><?php echo esc_attr_e( 'Blog', 'caffeinebuilt' ); ?></a>
						</li>
						<li>
							<a href="<?php echo esc_url( site_url() ); ?>/hire-freelance-wordpress-developer/" title="<?php echo esc_attr_e( 'Hire me page', 'caffeinebuilt' ); ?>"><?php echo esc_attr_e( 'Hire me', 'caffeinebuilt' ); ?></a>
						</li>
					</ul>
				</div>
			</div>
		</nav>

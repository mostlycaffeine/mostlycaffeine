<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @package caffeinebuilt
 */
?>

<section class="content-wrapper no-results not-found">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Sorry, nothing found.', 'caffeinebuilt' ); ?></h1>
				</header>
				<?php
					if ( is_home() && current_user_can( 'publish_posts' ) ) :
				?>
					<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'caffeinebuilt' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>
				<?php
					elseif ( is_search() ) :
				?>
					<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'caffeinebuilt' ); ?></p>
				<?php
						get_search_form();
					else :
				?>
					<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'caffeinebuilt' ); ?></p>
				<?php
						get_search_form();
					endif;
				?>
			</div>
		</div>
	</div>
</section>

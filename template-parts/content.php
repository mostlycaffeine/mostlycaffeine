<?php
/**
 * Template part for displaying posts
 *
 * @package caffeinebuilt
 */

?>

<article id="post-<?php the_ID(); ?>">
	<div class="entry-content">
		<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>

		<?php
		if ( 'post' === get_post_type() ) :
		?>
			<div class="entry-meta">

				<?php cb_posted_on(); ?>

				<div class="categories-list">
					<ul>
						<?php the_category( ' ' ); ?>
					</ul>
				</div>
			</div>
		<?php
		endif;
		?>

			<div class="entry-excerpt medium-text">
				<?php the_excerpt(); ?>
			</div>

	</div>
</article>

<?php
/**
 * The template for displaying all single posts
 *
 * @package caffeinebuilt
 */

?>
<section class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'single' );

					the_post_navigation();

					// if ( comments_open() || get_comments_number() ) :

					// 	comments_template();

					// endif;

				endwhile;
				?>
			</div>
		</div>
	</div>
</section>

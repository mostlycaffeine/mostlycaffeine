<?php
/**
 * The sidebar containing the main widget area
 *
 * @package caffeinebuilt
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside class="sidebar-widgets col-xs-12 col-sm-12 col-md-3 col-lg-3" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside>

<?php
/**
 * The template for displaying search results pages
 *
 * @package caffeinebuilt
 */

?>

<section class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php
					if ( have_posts() ) :
				?>
					<header class="page-header">
						<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'caffeinebuilt' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
					</header>
					<?php
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content', 'search' );
						endwhile;
						the_posts_navigation();
					?>
				<?php
					else : 
						get_template_part( 'template-parts/content', 'none' );
					endif;
				?>
			</div>
		</div>
	</div>
</section>

<?php
get_template_part( 'template-parts/header' );
?>

	<div id="content" class="wrapper">
		<main id="main" class="site-main" role="main">
			<?php include cb_template_path(); ?>
		</main>
	</div>

<?php
get_template_part( 'template-parts/footer' );

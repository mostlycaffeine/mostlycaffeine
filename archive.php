<?php
/**
 * The template for displaying archive pages
 *
 * @package caffeinebuilt
 */

?>

<section class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<?php
				if ( have_posts() ) :
			?>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<header class="page-header">
						<?php
							the_archive_title( '<h1 class="page-title">', '</h1>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
						?>
					</header>
				</div>
				<?php
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content', get_post_type() );
					endwhile;
					the_posts_navigation();
				?>
			<?php
				else :
					get_template_part( 'template-parts/content', 'none' );
				endif;
			?>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>

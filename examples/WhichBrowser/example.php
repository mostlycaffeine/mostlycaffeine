<?php
/**
 * WhichBrowser
 * https://github.com/WhichBrowser/Parser-PHP
 */

if ( class_exists( 'WhichBrowser\Parser' ) ) {

	print_r( $wb_result );

	if ( $wb_result->isType( 'desktop' ) ) {
		echo 'desktop';
	}
	if ( $wb_result->isType( 'mobile' ) ) {
		echo 'mobile';
	}
	if ( $wb_result->isType( 'tablet' ) ) {
		echo 'tablet';
	}
	if ( $wb_result->isOs( 'OS X' ) ) {
		echo 'OS X';
	}

}


var infScroll = new InfiniteScroll( '.container', {
	path: function() {
		return 'https://mostlycaffeine2018.local/wp-json/wp/v2/posts?page=' + this.pageIndex;
	},
	responseType: 'text',
	status: '.scroll-status',
	history: false,
});
	
infScroll.on( 'load', function( response ) {
	var data = JSON.parse( response );

	for (var key in data) {
		if (data.hasOwnProperty(key)) {
			console.log(data[key].slug);
		}
	}

});

infScroll.loadNextPage();

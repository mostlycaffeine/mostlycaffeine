<?php
/**
 * Template Name: Services
 * The home page.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/
 *
 * @package caffeinebuilt
 */

$linkedin_url = get_field( 'linkedin_url', 'options' );

while ( have_posts() ) : the_post();
?>

<header class="section__opening page__opening">
	<div class="section__opening--inner">
		<h1>
			<span class="heading-bg">
				<?php the_title(); ?>
			</span>
		</h1>
	</div>
</header>

<?php
// Bring in Gutenberg etc.
the_content();
?>

<?php
endwhile;

get_template_part( 'template-parts/sections/section_links' );

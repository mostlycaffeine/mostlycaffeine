<?php
/**
 * Template Name: Home
 * The home page.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/
 *
 * @package caffeinebuilt
 */

get_template_part( 'template-parts/sections/section-opening' );
get_template_part( 'template-parts/sections/section-overview' );
get_template_part( 'template-parts/sections/section-about' );
get_template_part( 'template-parts/sections/section-clients' );
get_template_part( 'template-parts/sections/section-skills' );
get_template_part( 'template-parts/sections/section-quotes' );

<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package caffeinebuilt
 */

?>

<section class="error-404 not-found">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( '404, Not Found.', 'caffeinebuilt' ); ?></h1>
				</header>
				<div class="page-content">
					<p><?php esc_html_e( 'Sorry, but the page you were trying to view does not exist.', 'caffeinebuilt' ); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>

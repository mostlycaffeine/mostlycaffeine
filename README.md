# mostlycaffeine.com theme

This is the theme for my website: https://mostlycaffeine.com

If you are here to review my code then you can also check out my public plugins here:

- https://gitlab.com/mostlycaffeine/admin-cleanup-controls
- https://gitlab.com/mostlycaffeine/admin-dev-colours
- https://gitlab.com/mostlycaffeine/email-verify-signup



## Hire me :-)
Hey! I'm a [freelance WordPress developer and technical consultant](https://mostlycaffeine.com) with a mission to deliver accessible, performant, secure websites that are sustainable and respect human rights. I'm available to hire for full projects or contract work, and would love to work with you!
- https://mostlycaffeine.com
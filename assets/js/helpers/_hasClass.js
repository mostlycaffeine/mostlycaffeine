/**
* helper function to check whether an element has a specific class
* @param {HTMLElement} el the element
* @param {string} className the class
* @return {boolean} whether the element has the class
*/
function hasClass(el, className) {
	return el.classList ? el.classList.contains(className) : new RegExp('\\b' + className + '\\b').test(el.className);
}
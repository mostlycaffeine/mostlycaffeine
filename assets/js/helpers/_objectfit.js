/**
 * <div class="objectfit__wrapper">
 * 		<img src="http://placehold.it/100x100" class="objectfit__image" data-src="http://placehold.it/100x100">
 * </div>
 */

var objectfitImgWrappers;
var len;

if ( ! Modernizr.objectfit ) {
	objectfitImgWrappers = document.querySelectorAll('.objectfit__wrapper');
	len = objectfitImgWrappers.length;

	for ( var i=0; i<len; i++ ) {
		var $wrapper = objectfitImgWrappers[i],
			imgUrl = $wrapper.querySelector('img').getAttribute('data-src');
		if (imgUrl) {
			$wrapper.style.backgroundImage = 'url(' + imgUrl + ')';
			addClass($wrapper, 'compat-object-fit');
		}
	}
}
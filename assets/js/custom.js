"use strict";

(function () {
  var SITE = {
    'common': {
      init: function init() {
        console.log('common');
        /** If objectfit is not supported then grab fallback image and set as background. */
        // if ( ! Modernizr.objectfit ) {
        // 	$('.objectfit').each(function () {
        // 	  var $container = $(this),
        // 		  imgUrl = $container.find('img').prop('src');
        // 	  if (imgUrl) {
        // 		$container
        // 		  .css('backgroundImage', 'url(' + imgUrl + ')')
        // 		  .addClass('compat-object-fit');
        // 	  }
        // 	});
        // }

        /** End objectfit */
      },
      finalize: function finalize() {
        console.log('finalize_common');
      }
    },
    'home': {
      init: function init() {
        console.log('home');
      },
      finalize: function finalize() {
        console.log('finalize_home');
      }
    },
    'blog': {
      init: function init() {
        console.log('blog');
      },
      finalize: function finalize() {
        console.log('finalize_blog');
      }
    },
    'about_us': {
      init: function init() {
        console.log('about_us');
      }
    }
  };
  var APP = {
    init_JS: function init_JS(func, funcname, args) {
      var fire;
      var namespace = SITE;
      funcname = funcname === undefined ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    domReady: function domReady() {
      APP.init_JS('common');
      document.body.className.replace(/-/g, '_').split(/\s+/).forEach(function (bodyClass) {
        APP.init_JS(bodyClass);
        APP.init_JS(bodyClass, 'finalize');
      });
      APP.init_JS('common', 'finalize');
    }
  };

  if (document.readyState !== 'loading') {
    APP.domReady;
  } else {
    document.addEventListener('DOMContentLoaded', APP.domReady);
  }
})();
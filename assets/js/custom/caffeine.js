(function () {
    var SITE = {
        'common': {
            init: function () {
				console.log('common');

				/** If objectfit is not supported then grab fallback image and set as background. */
				// if ( ! Modernizr.objectfit ) {
				// 	$('.objectfit').each(function () {
				// 	  var $container = $(this),
				// 		  imgUrl = $container.find('img').prop('src');
				// 	  if (imgUrl) {
				// 		$container
				// 		  .css('backgroundImage', 'url(' + imgUrl + ')')
				// 		  .addClass('compat-object-fit');
				// 	  }
				// 	});
				// }
				/** End objectfit */

			},
            finalize: function () {
                console.log('finalize_common');
            }
        },
        'home': {
            init: function () {
                console.log('home');
            },
            finalize: function () {
                console.log('finalize_home');
            }
        },
        'blog': {
            init: function () {
                console.log('blog');
            },
            finalize: function () {
                console.log('finalize_blog');
            }
        },
        'about_us': {
            init: function () {
                console.log('about_us');
            }
        }
    };

    var APP = {
        init_JS: function (func, funcname, args) {
            var fire;
            var namespace = SITE;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        domReady: function () {
            APP.init_JS('common');
            document.body.className.replace(/-/g, '_').split(/\s+/).forEach(function (bodyClass) {
                APP.init_JS(bodyClass);
                APP.init_JS(bodyClass, 'finalize');
            });

            APP.init_JS('common', 'finalize');
        }
    };


    if (document.readyState !== 'loading') {
        APP.domReady;
    } else {
        document.addEventListener('DOMContentLoaded', APP.domReady);
    }

})();

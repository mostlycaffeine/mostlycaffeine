"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*! lazysizes - v4.1.4 */
!function (a, b) {
  var c = b(a, a.document);
  a.lazySizes = c, "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports && (module.exports = c);
}(window, function (a, b) {
  "use strict";

  if (b.getElementsByClassName) {
    var c,
        d,
        e = b.documentElement,
        f = a.Date,
        g = a.HTMLPictureElement,
        h = "addEventListener",
        i = "getAttribute",
        j = a[h],
        k = a.setTimeout,
        l = a.requestAnimationFrame || k,
        m = a.requestIdleCallback,
        n = /^picture$/i,
        o = ["load", "error", "lazyincluded", "_lazyloaded"],
        p = {},
        q = Array.prototype.forEach,
        r = function r(a, b) {
      return p[b] || (p[b] = new RegExp("(\\s|^)" + b + "(\\s|$)")), p[b].test(a[i]("class") || "") && p[b];
    },
        s = function s(a, b) {
      r(a, b) || a.setAttribute("class", (a[i]("class") || "").trim() + " " + b);
    },
        t = function t(a, b) {
      var c;
      (c = r(a, b)) && a.setAttribute("class", (a[i]("class") || "").replace(c, " "));
    },
        u = function u(a, b, c) {
      var d = c ? h : "removeEventListener";
      c && u(a, b), o.forEach(function (c) {
        a[d](c, b);
      });
    },
        v = function v(a, d, e, f, g) {
      var h = b.createEvent("Event");
      return e || (e = {}), e.instance = c, h.initEvent(d, !f, !g), h.detail = e, a.dispatchEvent(h), h;
    },
        w = function w(b, c) {
      var e;
      !g && (e = a.picturefill || d.pf) ? (c && c.src && !b[i]("srcset") && b.setAttribute("srcset", c.src), e({
        reevaluate: !0,
        elements: [b]
      })) : c && c.src && (b.src = c.src);
    },
        x = function x(a, b) {
      return (getComputedStyle(a, null) || {})[b];
    },
        y = function y(a, b, c) {
      for (c = c || a.offsetWidth; c < d.minSize && b && !a._lazysizesWidth;) {
        c = b.offsetWidth, b = b.parentNode;
      }

      return c;
    },
        z = function () {
      var a,
          c,
          d = [],
          e = [],
          f = d,
          g = function g() {
        var b = f;

        for (f = d.length ? e : d, a = !0, c = !1; b.length;) {
          b.shift()();
        }

        a = !1;
      },
          h = function h(d, e) {
        a && !e ? d.apply(this, arguments) : (f.push(d), c || (c = !0, (b.hidden ? k : l)(g)));
      };

      return h._lsFlush = g, h;
    }(),
        A = function A(a, b) {
      return b ? function () {
        z(a);
      } : function () {
        var b = this,
            c = arguments;
        z(function () {
          a.apply(b, c);
        });
      };
    },
        B = function B(a) {
      var b,
          c = 0,
          e = d.throttleDelay,
          g = d.ricTimeout,
          h = function h() {
        b = !1, c = f.now(), a();
      },
          i = m && g > 49 ? function () {
        m(h, {
          timeout: g
        }), g !== d.ricTimeout && (g = d.ricTimeout);
      } : A(function () {
        k(h);
      }, !0);

      return function (a) {
        var d;
        (a = a === !0) && (g = 33), b || (b = !0, d = e - (f.now() - c), 0 > d && (d = 0), a || 9 > d ? i() : k(i, d));
      };
    },
        C = function C(a) {
      var b,
          c,
          d = 99,
          e = function e() {
        b = null, a();
      },
          g = function g() {
        var a = f.now() - c;
        d > a ? k(g, d - a) : (m || e)(e);
      };

      return function () {
        c = f.now(), b || (b = k(g, d));
      };
    };

    !function () {
      var b,
          c = {
        lazyClass: "lazyload",
        loadedClass: "lazyloaded",
        loadingClass: "lazyloading",
        preloadClass: "lazypreload",
        errorClass: "lazyerror",
        autosizesClass: "lazyautosizes",
        srcAttr: "data-src",
        srcsetAttr: "data-srcset",
        sizesAttr: "data-sizes",
        minSize: 40,
        customMedia: {},
        init: !0,
        expFactor: 1.5,
        hFac: .8,
        loadMode: 2,
        loadHidden: !0,
        ricTimeout: 0,
        throttleDelay: 125
      };
      d = a.lazySizesConfig || a.lazysizesConfig || {};

      for (b in c) {
        b in d || (d[b] = c[b]);
      }

      a.lazySizesConfig = d, k(function () {
        d.init && F();
      });
    }();

    var D = function () {
      var g,
          l,
          m,
          o,
          p,
          y,
          D,
          F,
          G,
          H,
          I,
          J,
          K,
          L,
          M = /^img$/i,
          N = /^iframe$/i,
          O = "onscroll" in a && !/(gle|ing)bot/.test(navigator.userAgent),
          P = 0,
          Q = 0,
          R = 0,
          S = -1,
          T = function T(a) {
        R--, a && a.target && u(a.target, T), (!a || 0 > R || !a.target) && (R = 0);
      },
          U = function U(a, c) {
        var d,
            f = a,
            g = "hidden" == x(b.body, "visibility") || "hidden" != x(a.parentNode, "visibility") && "hidden" != x(a, "visibility");

        for (F -= c, I += c, G -= c, H += c; g && (f = f.offsetParent) && f != b.body && f != e;) {
          g = (x(f, "opacity") || 1) > 0, g && "visible" != x(f, "overflow") && (d = f.getBoundingClientRect(), g = H > d.left && G < d.right && I > d.top - 1 && F < d.bottom + 1);
        }

        return g;
      },
          V = function V() {
        var a,
            f,
            h,
            j,
            k,
            m,
            n,
            p,
            q,
            r = c.elements;

        if ((o = d.loadMode) && 8 > R && (a = r.length)) {
          f = 0, S++, null == K && ("expand" in d || (d.expand = e.clientHeight > 500 && e.clientWidth > 500 ? 500 : 370), J = d.expand, K = J * d.expFactor), K > Q && 1 > R && S > 2 && o > 2 && !b.hidden ? (Q = K, S = 0) : Q = o > 1 && S > 1 && 6 > R ? J : P;

          for (; a > f; f++) {
            if (r[f] && !r[f]._lazyRace) if (O) {
              if ((p = r[f][i]("data-expand")) && (m = 1 * p) || (m = Q), q !== m && (y = innerWidth + m * L, D = innerHeight + m, n = -1 * m, q = m), h = r[f].getBoundingClientRect(), (I = h.bottom) >= n && (F = h.top) <= D && (H = h.right) >= n * L && (G = h.left) <= y && (I || H || G || F) && (d.loadHidden || "hidden" != x(r[f], "visibility")) && (l && 3 > R && !p && (3 > o || 4 > S) || U(r[f], m))) {
                if (ba(r[f]), k = !0, R > 9) break;
              } else !k && l && !j && 4 > R && 4 > S && o > 2 && (g[0] || d.preloadAfterLoad) && (g[0] || !p && (I || H || G || F || "auto" != r[f][i](d.sizesAttr))) && (j = g[0] || r[f]);
            } else ba(r[f]);
          }

          j && !k && ba(j);
        }
      },
          W = B(V),
          X = function X(a) {
        s(a.target, d.loadedClass), t(a.target, d.loadingClass), u(a.target, Z), v(a.target, "lazyloaded");
      },
          Y = A(X),
          Z = function Z(a) {
        Y({
          target: a.target
        });
      },
          $ = function $(a, b) {
        try {
          a.contentWindow.location.replace(b);
        } catch (c) {
          a.src = b;
        }
      },
          _ = function _(a) {
        var b,
            c = a[i](d.srcsetAttr);
        (b = d.customMedia[a[i]("data-media") || a[i]("media")]) && a.setAttribute("media", b), c && a.setAttribute("srcset", c);
      },
          aa = A(function (a, b, c, e, f) {
        var g, h, j, l, o, p;
        (o = v(a, "lazybeforeunveil", b)).defaultPrevented || (e && (c ? s(a, d.autosizesClass) : a.setAttribute("sizes", e)), h = a[i](d.srcsetAttr), g = a[i](d.srcAttr), f && (j = a.parentNode, l = j && n.test(j.nodeName || "")), p = b.firesLoad || "src" in a && (h || g || l), o = {
          target: a
        }, p && (u(a, T, !0), clearTimeout(m), m = k(T, 2500), s(a, d.loadingClass), u(a, Z, !0)), l && q.call(j.getElementsByTagName("source"), _), h ? a.setAttribute("srcset", h) : g && !l && (N.test(a.nodeName) ? $(a, g) : a.src = g), f && (h || l) && w(a, {
          src: g
        })), a._lazyRace && delete a._lazyRace, t(a, d.lazyClass), z(function () {
          (!p || a.complete && a.naturalWidth > 1) && (p ? T(o) : R--, X(o));
        }, !0);
      }),
          ba = function ba(a) {
        var b,
            c = M.test(a.nodeName),
            e = c && (a[i](d.sizesAttr) || a[i]("sizes")),
            f = "auto" == e;
        (!f && l || !c || !a[i]("src") && !a.srcset || a.complete || r(a, d.errorClass) || !r(a, d.lazyClass)) && (b = v(a, "lazyunveilread").detail, f && E.updateElem(a, !0, a.offsetWidth), a._lazyRace = !0, R++, aa(a, b, f, e, c));
      },
          ca = function ca() {
        if (!l) {
          if (f.now() - p < 999) return void k(ca, 999);
          var a = C(function () {
            d.loadMode = 3, W();
          });
          l = !0, d.loadMode = 3, W(), j("scroll", function () {
            3 == d.loadMode && (d.loadMode = 2), a();
          }, !0);
        }
      };

      return {
        _: function _() {
          p = f.now(), c.elements = b.getElementsByClassName(d.lazyClass), g = b.getElementsByClassName(d.lazyClass + " " + d.preloadClass), L = d.hFac, j("scroll", W, !0), j("resize", W, !0), a.MutationObserver ? new MutationObserver(W).observe(e, {
            childList: !0,
            subtree: !0,
            attributes: !0
          }) : (e[h]("DOMNodeInserted", W, !0), e[h]("DOMAttrModified", W, !0), setInterval(W, 999)), j("hashchange", W, !0), ["focus", "mouseover", "click", "load", "transitionend", "animationend", "webkitAnimationEnd"].forEach(function (a) {
            b[h](a, W, !0);
          }), /d$|^c/.test(b.readyState) ? ca() : (j("load", ca), b[h]("DOMContentLoaded", W), k(ca, 2e4)), c.elements.length ? (V(), z._lsFlush()) : W();
        },
        checkElems: W,
        unveil: ba
      };
    }(),
        E = function () {
      var a,
          c = A(function (a, b, c, d) {
        var e, f, g;
        if (a._lazysizesWidth = d, d += "px", a.setAttribute("sizes", d), n.test(b.nodeName || "")) for (e = b.getElementsByTagName("source"), f = 0, g = e.length; g > f; f++) {
          e[f].setAttribute("sizes", d);
        }
        c.detail.dataAttr || w(a, c.detail);
      }),
          e = function e(a, b, d) {
        var e,
            f = a.parentNode;
        f && (d = y(a, f, d), e = v(a, "lazybeforesizes", {
          width: d,
          dataAttr: !!b
        }), e.defaultPrevented || (d = e.detail.width, d && d !== a._lazysizesWidth && c(a, f, e, d)));
      },
          f = function f() {
        var b,
            c = a.length;
        if (c) for (b = 0; c > b; b++) {
          e(a[b]);
        }
      },
          g = C(f);

      return {
        _: function _() {
          a = b.getElementsByClassName(d.autosizesClass), j("resize", g);
        },
        checkElems: g,
        updateElem: e
      };
    }(),
        F = function F() {
      F.i || (F.i = !0, E._(), D._());
    };

    return c = {
      cfg: d,
      autoSizer: E,
      loader: D,
      init: F,
      uP: w,
      aC: s,
      rC: t,
      hC: r,
      fire: v,
      gW: y,
      rAF: z
    };
  }
});
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*! lazysizes - v4.1.4 */
!function (a, b) {
  var c = function c() {
    b(a.lazySizes), a.removeEventListener("lazyunveilread", c, !0);
  };

  b = b.bind(null, a, a.document), "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? b(require("lazysizes"), require("../fix-ios-sizes/fix-ios-sizes")) : a.lazySizes ? c() : a.addEventListener("lazyunveilread", c, !0);
}(window, function (a, b, c) {
  "use strict";

  var d,
      e = c && c.cfg || a.lazySizesConfig,
      f = b.createElement("img"),
      g = "sizes" in f && "srcset" in f,
      h = /\s+\d+h/g,
      i = function () {
    var a = /\s+(\d+)(w|h)\s+(\d+)(w|h)/,
        c = Array.prototype.forEach;
    return function () {
      var d = b.createElement("img"),
          e = function e(b) {
        var c,
            d,
            e = b.getAttribute(lazySizesConfig.srcsetAttr);
        e && ((d = e.match(a)) && (c = "w" == d[2] ? d[1] / d[3] : d[3] / d[1], c && b.setAttribute("data-aspectratio", c)), b.setAttribute(lazySizesConfig.srcsetAttr, e.replace(h, "")));
      },
          f = function f(a) {
        var b = a.target.parentNode;
        b && "PICTURE" == b.nodeName && c.call(b.getElementsByTagName("source"), e), e(a.target);
      },
          g = function g() {
        d.currentSrc && b.removeEventListener("lazybeforeunveil", f);
      };

      b.addEventListener("lazybeforeunveil", f), d.onload = g, d.onerror = g, d.srcset = "data:,a 1w 1h", d.complete && g();
    };
  }();

  if (e || (e = {}, a.lazySizesConfig = e), e.supportsType || (e.supportsType = function (a) {
    return !a;
  }), !a.picturefill && !e.pf) {
    if (a.HTMLPictureElement && g) return b.msElementsFromPoint && i(navigator.userAgent.match(/Edge\/(\d+)/)), void (e.pf = function () {});
    e.pf = function (b) {
      var c, e;
      if (!a.picturefill) for (c = 0, e = b.elements.length; e > c; c++) {
        d(b.elements[c]);
      }
    }, d = function () {
      var f = function f(a, b) {
        return a.w - b.w;
      },
          i = /^\s*\d+\.*\d*px\s*$/,
          j = function j(a) {
        var b,
            c,
            d = a.length,
            e = a[d - 1],
            f = 0;

        for (f; d > f; f++) {
          if (e = a[f], e.d = e.w / a.w, e.d >= a.d) {
            !e.cached && (b = a[f - 1]) && b.d > a.d - .13 * Math.pow(a.d, 2.2) && (c = Math.pow(b.d - .6, 1.6), b.cached && (b.d += .15 * c), b.d + (e.d - a.d) * c > a.d && (e = b));
            break;
          }
        }

        return e;
      },
          k = function () {
        var a,
            b = /(([^,\s].[^\s]+)\s+(\d+)w)/g,
            c = /\s/,
            d = function d(b, c, _d, e) {
          a.push({
            c: c,
            u: _d,
            w: 1 * e
          });
        };

        return function (e) {
          return a = [], e = e.trim(), e.replace(h, "").replace(b, d), a.length || !e || c.test(e) || a.push({
            c: e,
            u: e,
            w: 99
          }), a;
        };
      }(),
          l = function l() {
        l.init || (l.init = !0, addEventListener("resize", function () {
          var a,
              c = b.getElementsByClassName("lazymatchmedia"),
              e = function e() {
            var a, b;

            for (a = 0, b = c.length; b > a; a++) {
              d(c[a]);
            }
          };

          return function () {
            clearTimeout(a), a = setTimeout(e, 66);
          };
        }()));
      },
          m = function m(b, d) {
        var f,
            g = b.getAttribute("srcset") || b.getAttribute(e.srcsetAttr);
        !g && d && (g = b._lazypolyfill ? b._lazypolyfill._set : b.getAttribute(e.srcAttr) || b.getAttribute("src")), b._lazypolyfill && b._lazypolyfill._set == g || (f = k(g || ""), d && b.parentNode && (f.isPicture = "PICTURE" == b.parentNode.nodeName.toUpperCase(), f.isPicture && a.matchMedia && (c.aC(b, "lazymatchmedia"), l())), f._set = g, Object.defineProperty(b, "_lazypolyfill", {
          value: f,
          writable: !0
        }));
      },
          n = function n(b) {
        var d = a.devicePixelRatio || 1,
            e = c.getX && c.getX(b);
        return Math.min(e || d, 2.5, d);
      },
          _o = function o(b) {
        return a.matchMedia ? (_o = function o(a) {
          return !a || (matchMedia(a) || {}).matches;
        })(b) : !b;
      },
          p = function p(a) {
        var b, d, g, h, k, l, p;
        if (h = a, m(h, !0), k = h._lazypolyfill, k.isPicture) for (d = 0, b = a.parentNode.getElementsByTagName("source"), g = b.length; g > d; d++) {
          if (e.supportsType(b[d].getAttribute("type"), a) && _o(b[d].getAttribute("media"))) {
            h = b[d], m(h), k = h._lazypolyfill;
            break;
          }
        }
        return k.length > 1 ? (p = h.getAttribute("sizes") || "", p = i.test(p) && parseInt(p, 10) || c.gW(a, a.parentNode), k.d = n(a), !k.src || !k.w || k.w < p ? (k.w = p, l = j(k.sort(f)), k.src = l) : l = k.src) : l = k[0], l;
      },
          q = function q(a) {
        if (!g || !a.parentNode || "PICTURE" == a.parentNode.nodeName.toUpperCase()) {
          var b = p(a);
          b && b.u && a._lazypolyfill.cur != b.u && (a._lazypolyfill.cur = b.u, b.cached = !0, a.setAttribute(e.srcAttr, b.u), a.setAttribute("src", b.u));
        }
      };

      return q.parse = k, q;
    }(), e.loadedClass && e.loadingClass && !function () {
      var a = [];
      ['img[sizes$="px"][srcset].', "picture > img:not([srcset])."].forEach(function (b) {
        a.push(b + e.loadedClass), a.push(b + e.loadingClass);
      }), e.pf({
        elements: b.querySelectorAll(a.join(", "))
      });
    }();
  }
});
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*! lazysizes - v4.1.4 */
!function (a, b) {
  var c = function c() {
    b(a.lazySizes), a.removeEventListener("lazyunveilread", c, !0);
  };

  b = b.bind(null, a, a.document), "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? b(require("lazysizes")) : a.lazySizes ? c() : a.addEventListener("lazyunveilread", c, !0);
}(window, function (a, b, c) {
  "use strict";

  if (a.addEventListener) {
    var d = /\s+/g,
        e = /\s*\|\s+|\s+\|\s*/g,
        f = /^(.+?)(?:\s+\[\s*(.+?)\s*\])(?:\s+\[\s*(.+?)\s*\])?$/,
        g = /^\s*\(*\s*type\s*:\s*(.+?)\s*\)*\s*$/,
        h = /\(|\)|'/,
        i = {
      contain: 1,
      cover: 1
    },
        j = function j(a) {
      var b = c.gW(a, a.parentNode);
      return (!a._lazysizesWidth || b > a._lazysizesWidth) && (a._lazysizesWidth = b), a._lazysizesWidth;
    },
        k = function k(a) {
      var b;
      return b = (getComputedStyle(a) || {
        getPropertyValue: function getPropertyValue() {}
      }).getPropertyValue("background-size"), !i[b] && i[a.style.backgroundSize] && (b = a.style.backgroundSize), b;
    },
        l = function l(a, b) {
      if (b) {
        var c = b.match(g);
        c && c[1] ? a.setAttribute("type", c[1]) : a.setAttribute("media", lazySizesConfig.customMedia[b] || b);
      }
    },
        m = function m(a, c, g) {
      var h = b.createElement("picture"),
          i = c.getAttribute(lazySizesConfig.sizesAttr),
          j = c.getAttribute("data-ratio"),
          k = c.getAttribute("data-optimumx");
      c._lazybgset && c._lazybgset.parentNode == c && c.removeChild(c._lazybgset), Object.defineProperty(g, "_lazybgset", {
        value: c,
        writable: !0
      }), Object.defineProperty(c, "_lazybgset", {
        value: h,
        writable: !0
      }), a = a.replace(d, " ").split(e), h.style.display = "none", g.className = lazySizesConfig.lazyClass, 1 != a.length || i || (i = "auto"), a.forEach(function (a) {
        var c,
            d = b.createElement("source");
        i && "auto" != i && d.setAttribute("sizes", i), (c = a.match(f)) ? (d.setAttribute(lazySizesConfig.srcsetAttr, c[1]), l(d, c[2]), l(d, c[3])) : d.setAttribute(lazySizesConfig.srcsetAttr, a), h.appendChild(d);
      }), i && (g.setAttribute(lazySizesConfig.sizesAttr, i), c.removeAttribute(lazySizesConfig.sizesAttr), c.removeAttribute("sizes")), k && g.setAttribute("data-optimumx", k), j && g.setAttribute("data-ratio", j), h.appendChild(g), c.appendChild(h);
    },
        n = function n(a) {
      if (a.target._lazybgset) {
        var b = a.target,
            d = b._lazybgset,
            e = b.currentSrc || b.src;

        if (e) {
          var f = c.fire(d, "bgsetproxy", {
            src: e,
            useSrc: h.test(e) ? JSON.stringify(e) : e
          });
          f.defaultPrevented || (d.style.backgroundImage = "url(" + f.detail.useSrc + ")");
        }

        b._lazybgsetLoading && (c.fire(d, "_lazyloaded", {}, !1, !0), delete b._lazybgsetLoading);
      }
    };

    addEventListener("lazybeforeunveil", function (a) {
      var d, e, f;
      !a.defaultPrevented && (d = a.target.getAttribute("data-bgset")) && (f = a.target, e = b.createElement("img"), e.alt = "", e._lazybgsetLoading = !0, a.detail.firesLoad = !0, m(d, f, e), setTimeout(function () {
        c.loader.unveil(e), c.rAF(function () {
          c.fire(e, "_lazyloaded", {}, !0, !0), e.complete && n({
            target: e
          });
        });
      }));
    }), b.addEventListener("load", n, !0), a.addEventListener("lazybeforesizes", function (a) {
      if (a.detail.instance == c && a.target._lazybgset && a.detail.dataAttr) {
        var b = a.target._lazybgset,
            d = k(b);
        i[d] && (a.target._lazysizesParentFit = d, c.rAF(function () {
          a.target.setAttribute("data-parent-fit", d), a.target._lazysizesParentFit && delete a.target._lazysizesParentFit;
        }));
      }
    }, !0), b.documentElement.addEventListener("lazybeforesizes", function (a) {
      !a.defaultPrevented && a.target._lazybgset && a.detail.instance == c && (a.detail.width = j(a.target._lazybgset));
    });
  }
});
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * Infinite Scroll PACKAGED v3.0.5
 * Automatically add next page
 *
 * Licensed GPLv3 for open source use
 * or Infinite Scroll Commercial License for commercial use
 *
 * https://infinite-scroll.com
 * Copyright 2018 Metafizzy
 */

/**
 * Bridget makes jQuery widgets
 * v2.0.1
 * MIT license
 */

/* jshint browser: true, strict: true, undef: true, unused: true */
(function (window, factory) {
  // universal module definition

  /*jshint strict: false */

  /* globals define, module, require */
  if (typeof define == 'function' && define.amd) {
    // AMD
    define('jquery-bridget/jquery-bridget', ['jquery'], function (jQuery) {
      return factory(window, jQuery);
    });
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS
    module.exports = factory(window, require('jquery'));
  } else {
    // browser global
    window.jQueryBridget = factory(window, window.jQuery);
  }
})(window, function factory(window, jQuery) {
  'use strict'; // ----- utils ----- //

  var arraySlice = Array.prototype.slice; // helper function for logging errors
  // $.error breaks jQuery chaining

  var console = window.console;
  var logError = typeof console == 'undefined' ? function () {} : function (message) {
    console.error(message);
  }; // ----- jQueryBridget ----- //

  function jQueryBridget(namespace, PluginClass, $) {
    $ = $ || jQuery || window.jQuery;

    if (!$) {
      return;
    } // add option method -> $().plugin('option', {...})


    if (!PluginClass.prototype.option) {
      // option setter
      PluginClass.prototype.option = function (opts) {
        // bail out if not an object
        if (!$.isPlainObject(opts)) {
          return;
        }

        this.options = $.extend(true, this.options, opts);
      };
    } // make jQuery plugin


    $.fn[namespace] = function (arg0
    /*, arg1 */
    ) {
      if (typeof arg0 == 'string') {
        // method call $().plugin( 'methodName', { options } )
        // shift arguments by 1
        var args = arraySlice.call(arguments, 1);
        return methodCall(this, arg0, args);
      } // just $().plugin({ options })


      plainCall(this, arg0);
      return this;
    }; // $().plugin('methodName')


    function methodCall($elems, methodName, args) {
      var returnValue;
      var pluginMethodStr = '$().' + namespace + '("' + methodName + '")';
      $elems.each(function (i, elem) {
        // get instance
        var instance = $.data(elem, namespace);

        if (!instance) {
          logError(namespace + ' not initialized. Cannot call methods, i.e. ' + pluginMethodStr);
          return;
        }

        var method = instance[methodName];

        if (!method || methodName.charAt(0) == '_') {
          logError(pluginMethodStr + ' is not a valid method');
          return;
        } // apply method, get return value


        var value = method.apply(instance, args); // set return value if value is returned, use only first value

        returnValue = returnValue === undefined ? value : returnValue;
      });
      return returnValue !== undefined ? returnValue : $elems;
    }

    function plainCall($elems, options) {
      $elems.each(function (i, elem) {
        var instance = $.data(elem, namespace);

        if (instance) {
          // set options & init
          instance.option(options);

          instance._init();
        } else {
          // initialize new instance
          instance = new PluginClass(elem, options);
          $.data(elem, namespace, instance);
        }
      });
    }

    updateJQuery($);
  } // ----- updateJQuery ----- //
  // set $.bridget for v1 backwards compatibility


  function updateJQuery($) {
    if (!$ || $ && $.bridget) {
      return;
    }

    $.bridget = jQueryBridget;
  }

  updateJQuery(jQuery || window.jQuery); // -----  ----- //

  return jQueryBridget;
});
/**
 * EvEmitter v1.1.0
 * Lil' event emitter
 * MIT License
 */

/* jshint unused: true, undef: true, strict: true */


(function (global, factory) {
  // universal module definition

  /* jshint strict: false */

  /* globals define, module, window */
  if (typeof define == 'function' && define.amd) {
    // AMD - RequireJS
    define('ev-emitter/ev-emitter', factory);
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS - Browserify, Webpack
    module.exports = factory();
  } else {
    // Browser globals
    global.EvEmitter = factory();
  }
})(typeof window != 'undefined' ? window : void 0, function () {
  function EvEmitter() {}

  var proto = EvEmitter.prototype;

  proto.on = function (eventName, listener) {
    if (!eventName || !listener) {
      return;
    } // set events hash


    var events = this._events = this._events || {}; // set listeners array

    var listeners = events[eventName] = events[eventName] || []; // only add once

    if (listeners.indexOf(listener) == -1) {
      listeners.push(listener);
    }

    return this;
  };

  proto.once = function (eventName, listener) {
    if (!eventName || !listener) {
      return;
    } // add event


    this.on(eventName, listener); // set once flag
    // set onceEvents hash

    var onceEvents = this._onceEvents = this._onceEvents || {}; // set onceListeners object

    var onceListeners = onceEvents[eventName] = onceEvents[eventName] || {}; // set flag

    onceListeners[listener] = true;
    return this;
  };

  proto.off = function (eventName, listener) {
    var listeners = this._events && this._events[eventName];

    if (!listeners || !listeners.length) {
      return;
    }

    var index = listeners.indexOf(listener);

    if (index != -1) {
      listeners.splice(index, 1);
    }

    return this;
  };

  proto.emitEvent = function (eventName, args) {
    var listeners = this._events && this._events[eventName];

    if (!listeners || !listeners.length) {
      return;
    } // copy over to avoid interference if .off() in listener


    listeners = listeners.slice(0);
    args = args || []; // once stuff

    var onceListeners = this._onceEvents && this._onceEvents[eventName];

    for (var i = 0; i < listeners.length; i++) {
      var listener = listeners[i];
      var isOnce = onceListeners && onceListeners[listener];

      if (isOnce) {
        // remove listener
        // remove before trigger to prevent recursion
        this.off(eventName, listener); // unset once flag

        delete onceListeners[listener];
      } // trigger listener


      listener.apply(this, args);
    }

    return this;
  };

  proto.allOff = function () {
    delete this._events;
    delete this._onceEvents;
  };

  return EvEmitter;
});
/**
 * matchesSelector v2.0.2
 * matchesSelector( element, '.selector' )
 * MIT license
 */

/*jshint browser: true, strict: true, undef: true, unused: true */


(function (window, factory) {
  /*global define: false, module: false */
  'use strict'; // universal module definition

  if (typeof define == 'function' && define.amd) {
    // AMD
    define('desandro-matches-selector/matches-selector', factory);
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS
    module.exports = factory();
  } else {
    // browser global
    window.matchesSelector = factory();
  }
})(window, function factory() {
  'use strict';

  var matchesMethod = function () {
    var ElemProto = window.Element.prototype; // check for the standard method name first

    if (ElemProto.matches) {
      return 'matches';
    } // check un-prefixed


    if (ElemProto.matchesSelector) {
      return 'matchesSelector';
    } // check vendor prefixes


    var prefixes = ['webkit', 'moz', 'ms', 'o'];

    for (var i = 0; i < prefixes.length; i++) {
      var prefix = prefixes[i];
      var method = prefix + 'MatchesSelector';

      if (ElemProto[method]) {
        return method;
      }
    }
  }();

  return function matchesSelector(elem, selector) {
    return elem[matchesMethod](selector);
  };
});
/**
 * Fizzy UI utils v2.0.7
 * MIT license
 */

/*jshint browser: true, undef: true, unused: true, strict: true */


(function (window, factory) {
  // universal module definition

  /*jshint strict: false */

  /*globals define, module, require */
  if (typeof define == 'function' && define.amd) {
    // AMD
    define('fizzy-ui-utils/utils', ['desandro-matches-selector/matches-selector'], function (matchesSelector) {
      return factory(window, matchesSelector);
    });
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS
    module.exports = factory(window, require('desandro-matches-selector'));
  } else {
    // browser global
    window.fizzyUIUtils = factory(window, window.matchesSelector);
  }
})(window, function factory(window, matchesSelector) {
  var utils = {}; // ----- extend ----- //
  // extends objects

  utils.extend = function (a, b) {
    for (var prop in b) {
      a[prop] = b[prop];
    }

    return a;
  }; // ----- modulo ----- //


  utils.modulo = function (num, div) {
    return (num % div + div) % div;
  }; // ----- makeArray ----- //


  var arraySlice = Array.prototype.slice; // turn element or nodeList into an array

  utils.makeArray = function (obj) {
    if (Array.isArray(obj)) {
      // use object if already an array
      return obj;
    } // return empty array if undefined or null. #6


    if (obj === null || obj === undefined) {
      return [];
    }

    var isArrayLike = _typeof(obj) == 'object' && typeof obj.length == 'number';

    if (isArrayLike) {
      // convert nodeList to array
      return arraySlice.call(obj);
    } // array of single index


    return [obj];
  }; // ----- removeFrom ----- //


  utils.removeFrom = function (ary, obj) {
    var index = ary.indexOf(obj);

    if (index != -1) {
      ary.splice(index, 1);
    }
  }; // ----- getParent ----- //


  utils.getParent = function (elem, selector) {
    while (elem.parentNode && elem != document.body) {
      elem = elem.parentNode;

      if (matchesSelector(elem, selector)) {
        return elem;
      }
    }
  }; // ----- getQueryElement ----- //
  // use element as selector string


  utils.getQueryElement = function (elem) {
    if (typeof elem == 'string') {
      return document.querySelector(elem);
    }

    return elem;
  }; // ----- handleEvent ----- //
  // enable .ontype to trigger from .addEventListener( elem, 'type' )


  utils.handleEvent = function (event) {
    var method = 'on' + event.type;

    if (this[method]) {
      this[method](event);
    }
  }; // ----- filterFindElements ----- //


  utils.filterFindElements = function (elems, selector) {
    // make array of elems
    elems = utils.makeArray(elems);
    var ffElems = [];
    elems.forEach(function (elem) {
      // check that elem is an actual element
      if (!(elem instanceof HTMLElement)) {
        return;
      } // add elem if no selector


      if (!selector) {
        ffElems.push(elem);
        return;
      } // filter & find items if we have a selector
      // filter


      if (matchesSelector(elem, selector)) {
        ffElems.push(elem);
      } // find children


      var childElems = elem.querySelectorAll(selector); // concat childElems to filterFound array

      for (var i = 0; i < childElems.length; i++) {
        ffElems.push(childElems[i]);
      }
    });
    return ffElems;
  }; // ----- debounceMethod ----- //


  utils.debounceMethod = function (_class, methodName, threshold) {
    threshold = threshold || 100; // original method

    var method = _class.prototype[methodName];
    var timeoutName = methodName + 'Timeout';

    _class.prototype[methodName] = function () {
      var timeout = this[timeoutName];
      clearTimeout(timeout);
      var args = arguments;

      var _this = this;

      this[timeoutName] = setTimeout(function () {
        method.apply(_this, args);
        delete _this[timeoutName];
      }, threshold);
    };
  }; // ----- docReady ----- //


  utils.docReady = function (callback) {
    var readyState = document.readyState;

    if (readyState == 'complete' || readyState == 'interactive') {
      // do async to allow for other scripts to run. metafizzy/flickity#441
      setTimeout(callback);
    } else {
      document.addEventListener('DOMContentLoaded', callback);
    }
  }; // ----- htmlInit ----- //
  // http://jamesroberts.name/blog/2010/02/22/string-functions-for-javascript-trim-to-camel-case-to-dashed-and-to-underscore/


  utils.toDashed = function (str) {
    return str.replace(/(.)([A-Z])/g, function (match, $1, $2) {
      return $1 + '-' + $2;
    }).toLowerCase();
  };

  var console = window.console;
  /**
   * allow user to initialize classes via [data-namespace] or .js-namespace class
   * htmlInit( Widget, 'widgetName' )
   * options are parsed from data-namespace-options
   */

  utils.htmlInit = function (WidgetClass, namespace) {
    utils.docReady(function () {
      var dashedNamespace = utils.toDashed(namespace);
      var dataAttr = 'data-' + dashedNamespace;
      var dataAttrElems = document.querySelectorAll('[' + dataAttr + ']');
      var jsDashElems = document.querySelectorAll('.js-' + dashedNamespace);
      var elems = utils.makeArray(dataAttrElems).concat(utils.makeArray(jsDashElems));
      var dataOptionsAttr = dataAttr + '-options';
      var jQuery = window.jQuery;
      elems.forEach(function (elem) {
        var attr = elem.getAttribute(dataAttr) || elem.getAttribute(dataOptionsAttr);
        var options;

        try {
          options = attr && JSON.parse(attr);
        } catch (error) {
          // log error, do not initialize
          if (console) {
            console.error('Error parsing ' + dataAttr + ' on ' + elem.className + ': ' + error);
          }

          return;
        } // initialize


        var instance = new WidgetClass(elem, options); // make available via $().data('namespace')

        if (jQuery) {
          jQuery.data(elem, namespace, instance);
        }
      });
    });
  }; // -----  ----- //


  return utils;
}); // core


(function (window, factory) {
  // universal module definition

  /* globals define, module, require */
  if (typeof define == 'function' && define.amd) {
    // AMD
    define('infinite-scroll/js/core', ['ev-emitter/ev-emitter', 'fizzy-ui-utils/utils'], function (EvEmitter, utils) {
      return factory(window, EvEmitter, utils);
    });
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS
    module.exports = factory(window, require('ev-emitter'), require('fizzy-ui-utils'));
  } else {
    // browser global
    window.InfiniteScroll = factory(window, window.EvEmitter, window.fizzyUIUtils);
  }
})(window, function factory(window, EvEmitter, utils) {
  var jQuery = window.jQuery; // internal store of all InfiniteScroll intances

  var instances = {};

  function InfiniteScroll(element, options) {
    var queryElem = utils.getQueryElement(element);

    if (!queryElem) {
      console.error('Bad element for InfiniteScroll: ' + (queryElem || element));
      return;
    }

    element = queryElem; // do not initialize twice on same element

    if (element.infiniteScrollGUID) {
      var instance = instances[element.infiniteScrollGUID];
      instance.option(options);
      return instance;
    }

    this.element = element; // options

    this.options = utils.extend({}, InfiniteScroll.defaults);
    this.option(options); // add jQuery

    if (jQuery) {
      this.$element = jQuery(this.element);
    }

    this.create();
  } // defaults


  InfiniteScroll.defaults = {// path: null,
    // hideNav: null,
    // debug: false,
  }; // create & destroy methods

  InfiniteScroll.create = {};
  InfiniteScroll.destroy = {};
  var proto = InfiniteScroll.prototype; // inherit EvEmitter

  utils.extend(proto, EvEmitter.prototype); // --------------------------  -------------------------- //
  // globally unique identifiers

  var GUID = 0;

  proto.create = function () {
    // create core
    // add id for InfiniteScroll.data
    var id = this.guid = ++GUID;
    this.element.infiniteScrollGUID = id; // expando

    instances[id] = this; // associate via id
    // properties

    this.pageIndex = 1; // default to first page

    this.loadCount = 0;
    this.updateGetPath(); // bail if getPath not set, or returns falsey #776

    var hasPath = this.getPath && this.getPath();

    if (!hasPath) {
      console.error('Disabling InfiniteScroll');
      return;
    }

    this.updateGetAbsolutePath();
    this.log('initialized', [this.element.className]);
    this.callOnInit(); // create features

    for (var method in InfiniteScroll.create) {
      InfiniteScroll.create[method].call(this);
    }
  };

  proto.option = function (opts) {
    utils.extend(this.options, opts);
  }; // call onInit option, used for binding events on init


  proto.callOnInit = function () {
    var onInit = this.options.onInit;

    if (onInit) {
      onInit.call(this, this);
    }
  }; // ----- events ----- //


  proto.dispatchEvent = function (type, event, args) {
    this.log(type, args);
    var emitArgs = event ? [event].concat(args) : args;
    this.emitEvent(type, emitArgs); // trigger jQuery event

    if (!jQuery || !this.$element) {
      return;
    } // namespace jQuery event


    type += '.infiniteScroll';
    var $event = type;

    if (event) {
      // create jQuery event
      var jQEvent = jQuery.Event(event);
      jQEvent.type = type;
      $event = jQEvent;
    }

    this.$element.trigger($event, args);
  };

  var loggers = {
    initialized: function initialized(className) {
      return 'on ' + className;
    },
    request: function request(path) {
      return 'URL: ' + path;
    },
    load: function load(response, path) {
      return (response.title || '') + '. URL: ' + path;
    },
    error: function error(_error, path) {
      return _error + '. URL: ' + path;
    },
    append: function append(response, path, items) {
      return items.length + ' items. URL: ' + path;
    },
    last: function last(response, path) {
      return 'URL: ' + path;
    },
    history: function history(title, path) {
      return 'URL: ' + path;
    },
    pageIndex: function pageIndex(index, origin) {
      return 'current page determined to be: ' + index + ' from ' + origin;
    }
  }; // log events

  proto.log = function (type, args) {
    if (!this.options.debug) {
      return;
    }

    var message = '[InfiniteScroll] ' + type;
    var logger = loggers[type];

    if (logger) {
      message += '. ' + logger.apply(this, args);
    }

    console.log(message);
  }; // -------------------------- methods used amoung features -------------------------- //


  proto.updateMeasurements = function () {
    this.windowHeight = window.innerHeight;
    var rect = this.element.getBoundingClientRect();
    this.top = rect.top + window.pageYOffset;
  };

  proto.updateScroller = function () {
    var elementScroll = this.options.elementScroll;

    if (!elementScroll) {
      // default, use window
      this.scroller = window;
      return;
    } // if true, set to element, otherwise use option


    this.scroller = elementScroll === true ? this.element : utils.getQueryElement(elementScroll);

    if (!this.scroller) {
      throw 'Unable to find elementScroll: ' + elementScroll;
    }
  }; // -------------------------- page path -------------------------- //


  proto.updateGetPath = function () {
    var optPath = this.options.path;

    if (!optPath) {
      console.error('InfiniteScroll path option required. Set as: ' + optPath);
      return;
    } // function


    var type = _typeof(optPath);

    if (type == 'function') {
      this.getPath = optPath;
      return;
    } // template string: '/pages/{{#}}.html'


    var templateMatch = type == 'string' && optPath.match('{{#}}');

    if (templateMatch) {
      this.updateGetPathTemplate(optPath);
      return;
    } // selector: '.next-page-selector'


    this.updateGetPathSelector(optPath);
  };

  proto.updateGetPathTemplate = function (optPath) {
    // set getPath with template string
    this.getPath = function () {
      var nextIndex = this.pageIndex + 1;
      return optPath.replace('{{#}}', nextIndex);
    }.bind(this); // get pageIndex from location
    // convert path option into regex to look for pattern in location


    var regexString = optPath.replace('{{#}}', '(\\d\\d?\\d?)');
    var templateRe = new RegExp(regexString);
    var match = location.href.match(templateRe);

    if (match) {
      this.pageIndex = parseInt(match[1], 10);
      this.log('pageIndex', [this.pageIndex, 'template string']);
    }
  };

  var pathRegexes = [// WordPress & Tumblr - example.com/page/2
  // Jekyll - example.com/page2
  /^(.*?\/?page\/?)(\d\d?\d?)(.*?$)/, // Drupal - example.com/?page=1
  /^(.*?\/?\?page=)(\d\d?\d?)(.*?$)/, // catch all, last occurence of a number
  /(.*?)(\d\d?\d?)(?!.*\d)(.*?$)/];

  proto.updateGetPathSelector = function (optPath) {
    // parse href of link: '.next-page-link'
    var hrefElem = document.querySelector(optPath);

    if (!hrefElem) {
      console.error('Bad InfiniteScroll path option. Next link not found: ' + optPath);
      return;
    }

    var href = hrefElem.getAttribute('href'); // try matching href to pathRegexes patterns

    var pathParts, regex;

    for (var i = 0; href && i < pathRegexes.length; i++) {
      regex = pathRegexes[i];
      var match = href.match(regex);

      if (match) {
        pathParts = match.slice(1); // remove first part

        break;
      }
    }

    if (!pathParts) {
      console.error('InfiniteScroll unable to parse next link href: ' + href);
      return;
    }

    this.isPathSelector = true; // flag for checkLastPage()

    this.getPath = function () {
      var nextIndex = this.pageIndex + 1;
      return pathParts[0] + nextIndex + pathParts[2];
    }.bind(this); // get pageIndex from href


    this.pageIndex = parseInt(pathParts[1], 10) - 1;
    this.log('pageIndex', [this.pageIndex, 'next link']);
  };

  proto.updateGetAbsolutePath = function () {
    var path = this.getPath(); // path doesn't start with http or /

    var isAbsolute = path.match(/^http/) || path.match(/^\//);

    if (isAbsolute) {
      this.getAbsolutePath = this.getPath;
      return;
    }

    var pathname = location.pathname; // /foo/bar/index.html => /foo/bar

    var directory = pathname.substring(0, pathname.lastIndexOf('/'));

    this.getAbsolutePath = function () {
      return directory + '/' + this.getPath();
    };
  }; // -------------------------- nav -------------------------- //
  // hide navigation


  InfiniteScroll.create.hideNav = function () {
    var nav = utils.getQueryElement(this.options.hideNav);

    if (!nav) {
      return;
    }

    nav.style.display = 'none';
    this.nav = nav;
  };

  InfiniteScroll.destroy.hideNav = function () {
    if (this.nav) {
      this.nav.style.display = '';
    }
  }; // -------------------------- destroy -------------------------- //


  proto.destroy = function () {
    this.allOff(); // remove all event listeners
    // call destroy methods

    for (var method in InfiniteScroll.destroy) {
      InfiniteScroll.destroy[method].call(this);
    }

    delete this.element.infiniteScrollGUID;
    delete instances[this.guid]; // remove jQuery data. #807

    if (jQuery && this.$element) {
      jQuery.removeData(this.element, 'infiniteScroll');
    }
  }; // -------------------------- utilities -------------------------- //
  // https://remysharp.com/2010/07/21/throttling-function-calls


  InfiniteScroll.throttle = function (fn, threshold) {
    threshold = threshold || 200;
    var last, timeout;
    return function () {
      var now = +new Date();
      var args = arguments;

      var trigger = function () {
        last = now;
        fn.apply(this, args);
      }.bind(this);

      if (last && now < last + threshold) {
        // hold on to it
        clearTimeout(timeout);
        timeout = setTimeout(trigger, threshold);
      } else {
        trigger();
      }
    };
  };

  InfiniteScroll.data = function (elem) {
    elem = utils.getQueryElement(elem);
    var id = elem && elem.infiniteScrollGUID;
    return id && instances[id];
  }; // set internal jQuery, for Webpack + jQuery v3


  InfiniteScroll.setJQuery = function ($) {
    jQuery = $;
  }; // -------------------------- setup -------------------------- //


  utils.htmlInit(InfiniteScroll, 'infinite-scroll'); // add noop _init method for jQuery Bridget. #768

  proto._init = function () {};

  if (jQuery && jQuery.bridget) {
    jQuery.bridget('infiniteScroll', InfiniteScroll);
  } // --------------------------  -------------------------- //


  return InfiniteScroll;
}); // page-load


(function (window, factory) {
  // universal module definition

  /* globals define, module, require */
  if (typeof define == 'function' && define.amd) {
    // AMD
    define('infinite-scroll/js/page-load', ['./core'], function (InfiniteScroll) {
      return factory(window, InfiniteScroll);
    });
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS
    module.exports = factory(window, require('./core'));
  } else {
    // browser global
    factory(window, window.InfiniteScroll);
  }
})(window, function factory(window, InfiniteScroll) {
  var proto = InfiniteScroll.prototype; // InfiniteScroll.defaults.append = false;

  InfiniteScroll.defaults.loadOnScroll = true;
  InfiniteScroll.defaults.checkLastPage = true;
  InfiniteScroll.defaults.responseType = 'document'; // InfiniteScroll.defaults.prefill = false;
  // InfiniteScroll.defaults.outlayer = null;

  InfiniteScroll.create.pageLoad = function () {
    this.canLoad = true;
    this.on('scrollThreshold', this.onScrollThresholdLoad);
    this.on('load', this.checkLastPage);

    if (this.options.outlayer) {
      this.on('append', this.onAppendOutlayer);
    }
  };

  proto.onScrollThresholdLoad = function () {
    if (this.options.loadOnScroll) {
      this.loadNextPage();
    }
  };

  proto.loadNextPage = function () {
    if (this.isLoading || !this.canLoad) {
      return;
    }

    var path = this.getAbsolutePath();
    this.isLoading = true;

    var onLoad = function (response) {
      this.onPageLoad(response, path);
    }.bind(this);

    var onError = function (error) {
      this.onPageError(error, path);
    }.bind(this);

    request(path, this.options.responseType, onLoad, onError);
    this.dispatchEvent('request', null, [path]);
  };

  proto.onPageLoad = function (response, path) {
    // done loading if not appending
    if (!this.options.append) {
      this.isLoading = false;
    }

    this.pageIndex++;
    this.loadCount++;
    this.dispatchEvent('load', null, [response, path]);
    this.appendNextPage(response, path);
    return response;
  };

  proto.appendNextPage = function (response, path) {
    var optAppend = this.options.append; // do not append json

    var isDocument = this.options.responseType == 'document';

    if (!isDocument || !optAppend) {
      return;
    }

    var items = response.querySelectorAll(optAppend);
    var fragment = getItemsFragment(items);

    var appendReady = function () {
      this.appendItems(items, fragment);
      this.isLoading = false;
      this.dispatchEvent('append', null, [response, path, items]);
    }.bind(this); // TODO add hook for option to trigger appendReady


    if (this.options.outlayer) {
      this.appendOutlayerItems(fragment, appendReady);
    } else {
      appendReady();
    }
  };

  proto.appendItems = function (items, fragment) {
    if (!items || !items.length) {
      return;
    } // get fragment if not provided


    fragment = fragment || getItemsFragment(items);
    refreshScripts(fragment);
    this.element.appendChild(fragment);
  };

  function getItemsFragment(items) {
    // add items to fragment
    var fragment = document.createDocumentFragment();

    for (var i = 0; items && i < items.length; i++) {
      fragment.appendChild(items[i]);
    }

    return fragment;
  } // replace <script>s with copies so they load
  // <script>s added by InfiniteScroll will not load
  // similar to https://stackoverflow.com/questions/610995


  function refreshScripts(fragment) {
    var scripts = fragment.querySelectorAll('script');

    for (var i = 0; i < scripts.length; i++) {
      var script = scripts[i];
      var freshScript = document.createElement('script');
      copyAttributes(script, freshScript); // copy inner script code. #718, #782

      freshScript.innerHTML = script.innerHTML;
      script.parentNode.replaceChild(freshScript, script);
    }
  }

  function copyAttributes(fromNode, toNode) {
    var attrs = fromNode.attributes;

    for (var i = 0; i < attrs.length; i++) {
      var attr = attrs[i];
      toNode.setAttribute(attr.name, attr.value);
    }
  } // ----- outlayer ----- //


  proto.appendOutlayerItems = function (fragment, appendReady) {
    var imagesLoaded = InfiniteScroll.imagesLoaded || window.imagesLoaded;

    if (!imagesLoaded) {
      console.error('[InfiniteScroll] imagesLoaded required for outlayer option');
      this.isLoading = false;
      return;
    } // append once images loaded


    imagesLoaded(fragment, appendReady);
  };

  proto.onAppendOutlayer = function (response, path, items) {
    this.options.outlayer.appended(items);
  }; // ----- checkLastPage ----- //
  // check response for next element


  proto.checkLastPage = function (response, path) {
    var checkLastPage = this.options.checkLastPage;

    if (!checkLastPage) {
      return;
    }

    var pathOpt = this.options.path; // if path is function, check if next path is truthy

    if (typeof pathOpt == 'function') {
      var nextPath = this.getPath();

      if (!nextPath) {
        this.lastPageReached(response, path);
        return;
      }
    } // get selector from checkLastPage or path option


    var selector;

    if (typeof checkLastPage == 'string') {
      selector = checkLastPage;
    } else if (this.isPathSelector) {
      // path option is selector string
      selector = pathOpt;
    } // check last page for selector
    // bail if no selector or not document response


    if (!selector || !response.querySelector) {
      return;
    } // check if response has selector


    var nextElem = response.querySelector(selector);

    if (!nextElem) {
      this.lastPageReached(response, path);
    }
  };

  proto.lastPageReached = function (response, path) {
    this.canLoad = false;
    this.dispatchEvent('last', null, [response, path]);
  }; // ----- error ----- //


  proto.onPageError = function (error, path) {
    this.isLoading = false;
    this.canLoad = false;
    this.dispatchEvent('error', null, [error, path]);
    return error;
  }; // -------------------------- prefill -------------------------- //


  InfiniteScroll.create.prefill = function () {
    if (!this.options.prefill) {
      return;
    }

    var append = this.options.append;

    if (!append) {
      console.error('append option required for prefill. Set as :' + append);
      return;
    }

    this.updateMeasurements();
    this.updateScroller();
    this.isPrefilling = true;
    this.on('append', this.prefill);
    this.once('error', this.stopPrefill);
    this.once('last', this.stopPrefill);
    this.prefill();
  };

  proto.prefill = function () {
    var distance = this.getPrefillDistance();
    this.isPrefilling = distance >= 0;

    if (this.isPrefilling) {
      this.log('prefill');
      this.loadNextPage();
    } else {
      this.stopPrefill();
    }
  };

  proto.getPrefillDistance = function () {
    // element scroll
    if (this.options.elementScroll) {
      return this.scroller.clientHeight - this.scroller.scrollHeight;
    } // window


    return this.windowHeight - this.element.clientHeight;
  };

  proto.stopPrefill = function () {
    this.log('stopPrefill');
    this.off('append', this.prefill);
  }; // -------------------------- request -------------------------- //


  function request(url, responseType, onLoad, onError) {
    var req = new XMLHttpRequest();
    req.open('GET', url, true); // set responseType document to return DOM

    req.responseType = responseType || ''; // set X-Requested-With header to check that is ajax request

    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    req.onload = function () {
      if (req.status == 200) {
        onLoad(req.response);
      } else {
        // not 200 OK, error
        var error = new Error(req.statusText);
        onError(error);
      }
    }; // Handle network errors


    req.onerror = function () {
      var error = new Error('Network error requesting ' + url);
      onError(error);
    };

    req.send();
  } // --------------------------  -------------------------- //


  return InfiniteScroll;
}); // scroll-watch


(function (window, factory) {
  // universal module definition

  /* globals define, module, require */
  if (typeof define == 'function' && define.amd) {
    // AMD
    define('infinite-scroll/js/scroll-watch', ['./core', 'fizzy-ui-utils/utils'], function (InfiniteScroll, utils) {
      return factory(window, InfiniteScroll, utils);
    });
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS
    module.exports = factory(window, require('./core'), require('fizzy-ui-utils'));
  } else {
    // browser global
    factory(window, window.InfiniteScroll, window.fizzyUIUtils);
  }
})(window, function factory(window, InfiniteScroll, utils) {
  var proto = InfiniteScroll.prototype; // default options

  InfiniteScroll.defaults.scrollThreshold = 400; // InfiniteScroll.defaults.elementScroll = null;

  InfiniteScroll.create.scrollWatch = function () {
    // events
    this.pageScrollHandler = this.onPageScroll.bind(this);
    this.resizeHandler = this.onResize.bind(this);
    var scrollThreshold = this.options.scrollThreshold;
    var isEnable = scrollThreshold || scrollThreshold === 0;

    if (isEnable) {
      this.enableScrollWatch();
    }
  };

  InfiniteScroll.destroy.scrollWatch = function () {
    this.disableScrollWatch();
  };

  proto.enableScrollWatch = function () {
    if (this.isScrollWatching) {
      return;
    }

    this.isScrollWatching = true;
    this.updateMeasurements();
    this.updateScroller(); // TODO disable after error?

    this.on('last', this.disableScrollWatch);
    this.bindScrollWatchEvents(true);
  };

  proto.disableScrollWatch = function () {
    if (!this.isScrollWatching) {
      return;
    }

    this.bindScrollWatchEvents(false);
    delete this.isScrollWatching;
  };

  proto.bindScrollWatchEvents = function (isBind) {
    var addRemove = isBind ? 'addEventListener' : 'removeEventListener';
    this.scroller[addRemove]('scroll', this.pageScrollHandler);
    window[addRemove]('resize', this.resizeHandler);
  };

  proto.onPageScroll = InfiniteScroll.throttle(function () {
    var distance = this.getBottomDistance();

    if (distance <= this.options.scrollThreshold) {
      this.dispatchEvent('scrollThreshold');
    }
  });

  proto.getBottomDistance = function () {
    if (this.options.elementScroll) {
      return this.getElementBottomDistance();
    } else {
      return this.getWindowBottomDistance();
    }
  };

  proto.getWindowBottomDistance = function () {
    var bottom = this.top + this.element.clientHeight;
    var scrollY = window.pageYOffset + this.windowHeight;
    return bottom - scrollY;
  };

  proto.getElementBottomDistance = function () {
    var bottom = this.scroller.scrollHeight;
    var scrollY = this.scroller.scrollTop + this.scroller.clientHeight;
    return bottom - scrollY;
  };

  proto.onResize = function () {
    this.updateMeasurements();
  };

  utils.debounceMethod(InfiniteScroll, 'onResize', 150); // --------------------------  -------------------------- //

  return InfiniteScroll;
}); // history


(function (window, factory) {
  // universal module definition

  /* globals define, module, require */
  if (typeof define == 'function' && define.amd) {
    // AMD
    define('infinite-scroll/js/history', ['./core', 'fizzy-ui-utils/utils'], function (InfiniteScroll, utils) {
      return factory(window, InfiniteScroll, utils);
    });
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS
    module.exports = factory(window, require('./core'), require('fizzy-ui-utils'));
  } else {
    // browser global
    factory(window, window.InfiniteScroll, window.fizzyUIUtils);
  }
})(window, function factory(window, InfiniteScroll, utils) {
  var proto = InfiniteScroll.prototype;
  InfiniteScroll.defaults.history = 'replace'; // InfiniteScroll.defaults.historyTitle = false;

  var link = document.createElement('a'); // ----- create/destroy ----- //

  InfiniteScroll.create.history = function () {
    if (!this.options.history) {
      return;
    } // check for same origin


    link.href = this.getAbsolutePath(); // MS Edge does not have origin on link https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/12236493/

    var linkOrigin = link.origin || link.protocol + '//' + link.host;
    var isSameOrigin = linkOrigin == location.origin;

    if (!isSameOrigin) {
      console.error('[InfiniteScroll] cannot set history with different origin: ' + link.origin + ' on ' + location.origin + ' . History behavior disabled.');
      return;
    } // two ways to handle changing history


    if (this.options.append) {
      this.createHistoryAppend();
    } else {
      this.createHistoryPageLoad();
    }
  };

  proto.createHistoryAppend = function () {
    this.updateMeasurements();
    this.updateScroller(); // array of scroll positions of appended pages

    this.scrollPages = [{
      // first page
      top: 0,
      path: location.href,
      title: document.title
    }];
    this.scrollPageIndex = 0; // events

    this.scrollHistoryHandler = this.onScrollHistory.bind(this);
    this.unloadHandler = this.onUnload.bind(this);
    this.scroller.addEventListener('scroll', this.scrollHistoryHandler);
    this.on('append', this.onAppendHistory);
    this.bindHistoryAppendEvents(true);
  };

  proto.bindHistoryAppendEvents = function (isBind) {
    var addRemove = isBind ? 'addEventListener' : 'removeEventListener';
    this.scroller[addRemove]('scroll', this.scrollHistoryHandler);
    window[addRemove]('unload', this.unloadHandler);
  };

  proto.createHistoryPageLoad = function () {
    this.on('load', this.onPageLoadHistory);
  };

  InfiniteScroll.destroy.history = proto.destroyHistory = function () {
    var isHistoryAppend = this.options.history && this.options.append;

    if (isHistoryAppend) {
      this.bindHistoryAppendEvents(false);
    }
  }; // ----- append history ----- //


  proto.onAppendHistory = function (response, path, items) {
    // do not proceed if no items. #779
    if (!items || !items.length) {
      return;
    }

    var firstItem = items[0];
    var elemScrollY = this.getElementScrollY(firstItem); // resolve path

    link.href = path; // add page data to hash

    this.scrollPages.push({
      top: elemScrollY,
      path: link.href,
      title: response.title
    });
  };

  proto.getElementScrollY = function (elem) {
    if (this.options.elementScroll) {
      return this.getElementElementScrollY(elem);
    } else {
      return this.getElementWindowScrollY(elem);
    }
  };

  proto.getElementWindowScrollY = function (elem) {
    var rect = elem.getBoundingClientRect();
    return rect.top + window.pageYOffset;
  }; // wow, stupid name


  proto.getElementElementScrollY = function (elem) {
    return elem.offsetTop - this.top;
  };

  proto.onScrollHistory = function () {
    // cycle through positions, find biggest without going over
    var scrollViewY = this.getScrollViewY();
    var pageIndex, page;

    for (var i = 0; i < this.scrollPages.length; i++) {
      var scrollPage = this.scrollPages[i];

      if (scrollPage.top >= scrollViewY) {
        break;
      }

      pageIndex = i;
      page = scrollPage;
    } // set history if changed


    if (pageIndex != this.scrollPageIndex) {
      this.scrollPageIndex = pageIndex;
      this.setHistory(page.title, page.path);
    }
  };

  utils.debounceMethod(InfiniteScroll, 'onScrollHistory', 150);

  proto.getScrollViewY = function () {
    if (this.options.elementScroll) {
      return this.scroller.scrollTop + this.scroller.clientHeight / 2;
    } else {
      return window.pageYOffset + this.windowHeight / 2;
    }
  };

  proto.setHistory = function (title, path) {
    var optHistory = this.options.history;
    var historyMethod = optHistory && history[optHistory + 'State'];

    if (!historyMethod) {
      return;
    }

    history[optHistory + 'State'](null, title, path);

    if (this.options.historyTitle) {
      document.title = title;
    }

    this.dispatchEvent('history', null, [title, path]);
  }; // scroll to top to prevent initial scroll-reset after page refresh
  // https://stackoverflow.com/a/18633915/182183


  proto.onUnload = function () {
    var pageIndex = this.scrollPageIndex;

    if (pageIndex === 0) {
      return;
    } // calculate where scroll position would be on refresh


    var scrollPage = this.scrollPages[pageIndex];
    var scrollY = window.pageYOffset - scrollPage.top + this.top; // disable scroll event before setting scroll #679

    this.destroyHistory();
    scrollTo(0, scrollY);
  }; // ----- load history ----- //
  // update URL


  proto.onPageLoadHistory = function (response, path) {
    this.setHistory(response.title, path);
  }; // --------------------------  -------------------------- //


  return InfiniteScroll;
}); // button


(function (window, factory) {
  // universal module definition

  /* globals define, module, require */
  if (typeof define == 'function' && define.amd) {
    // AMD
    define('infinite-scroll/js/button', ['./core', 'fizzy-ui-utils/utils'], function (InfiniteScroll, utils) {
      return factory(window, InfiniteScroll, utils);
    });
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS
    module.exports = factory(window, require('./core'), require('fizzy-ui-utils'));
  } else {
    // browser global
    factory(window, window.InfiniteScroll, window.fizzyUIUtils);
  }
})(window, function factory(window, InfiniteScroll, utils) {
  // InfiniteScroll.defaults.button = null;
  InfiniteScroll.create.button = function () {
    var buttonElem = utils.getQueryElement(this.options.button);

    if (buttonElem) {
      this.button = new InfiniteScrollButton(buttonElem, this);
      return;
    }
  };

  InfiniteScroll.destroy.button = function () {
    if (this.button) {
      this.button.destroy();
    }
  }; // -------------------------- InfiniteScrollButton -------------------------- //


  function InfiniteScrollButton(element, infScroll) {
    this.element = element;
    this.infScroll = infScroll; // events

    this.clickHandler = this.onClick.bind(this);
    this.element.addEventListener('click', this.clickHandler);
    infScroll.on('request', this.disable.bind(this));
    infScroll.on('load', this.enable.bind(this));
    infScroll.on('error', this.hide.bind(this));
    infScroll.on('last', this.hide.bind(this));
  }

  InfiniteScrollButton.prototype.onClick = function (event) {
    event.preventDefault();
    this.infScroll.loadNextPage();
  };

  InfiniteScrollButton.prototype.enable = function () {
    this.element.removeAttribute('disabled');
  };

  InfiniteScrollButton.prototype.disable = function () {
    this.element.disabled = 'disabled';
  };

  InfiniteScrollButton.prototype.hide = function () {
    this.element.style.display = 'none';
  };

  InfiniteScrollButton.prototype.destroy = function () {
    this.element.removeEventListener('click', this.clickHandler);
  }; // --------------------------  -------------------------- //


  InfiniteScroll.Button = InfiniteScrollButton;
  return InfiniteScroll;
}); // status


(function (window, factory) {
  // universal module definition

  /* globals define, module, require */
  if (typeof define == 'function' && define.amd) {
    // AMD
    define('infinite-scroll/js/status', ['./core', 'fizzy-ui-utils/utils'], function (InfiniteScroll, utils) {
      return factory(window, InfiniteScroll, utils);
    });
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS
    module.exports = factory(window, require('./core'), require('fizzy-ui-utils'));
  } else {
    // browser global
    factory(window, window.InfiniteScroll, window.fizzyUIUtils);
  }
})(window, function factory(window, InfiniteScroll, utils) {
  var proto = InfiniteScroll.prototype; // InfiniteScroll.defaults.status = null;

  InfiniteScroll.create.status = function () {
    var statusElem = utils.getQueryElement(this.options.status);

    if (!statusElem) {
      return;
    } // elements


    this.statusElement = statusElem;
    this.statusEventElements = {
      request: statusElem.querySelector('.infinite-scroll-request'),
      error: statusElem.querySelector('.infinite-scroll-error'),
      last: statusElem.querySelector('.infinite-scroll-last')
    }; // events

    this.on('request', this.showRequestStatus);
    this.on('error', this.showErrorStatus);
    this.on('last', this.showLastStatus);
    this.bindHideStatus('on');
  };

  proto.bindHideStatus = function (bindMethod) {
    var hideEvent = this.options.append ? 'append' : 'load';
    this[bindMethod](hideEvent, this.hideAllStatus);
  };

  proto.showRequestStatus = function () {
    this.showStatus('request');
  };

  proto.showErrorStatus = function () {
    this.showStatus('error');
  };

  proto.showLastStatus = function () {
    this.showStatus('last'); // prevent last then append event race condition from showing last status #706

    this.bindHideStatus('off');
  };

  proto.showStatus = function (eventName) {
    show(this.statusElement);
    this.hideStatusEventElements();
    var eventElem = this.statusEventElements[eventName];
    show(eventElem);
  };

  proto.hideAllStatus = function () {
    hide(this.statusElement);
    this.hideStatusEventElements();
  };

  proto.hideStatusEventElements = function () {
    for (var type in this.statusEventElements) {
      var eventElem = this.statusEventElements[type];
      hide(eventElem);
    }
  }; // --------------------------  -------------------------- //


  function hide(elem) {
    setDisplay(elem, 'none');
  }

  function show(elem) {
    setDisplay(elem, 'block');
  }

  function setDisplay(elem, value) {
    if (elem) {
      elem.style.display = value;
    }
  } // --------------------------  -------------------------- //


  return InfiniteScroll;
});
/*!
 * Infinite Scroll v3.0.5
 * Automatically add next page
 *
 * Licensed GPLv3 for open source use
 * or Infinite Scroll Commercial License for commercial use
 *
 * https://infinite-scroll.com
 * Copyright 2018 Metafizzy
 */


(function (window, factory) {
  // universal module definition

  /* globals define, module, require */
  if (typeof define == 'function' && define.amd) {
    // AMD
    define(['infinite-scroll/js/core', 'infinite-scroll/js/page-load', 'infinite-scroll/js/scroll-watch', 'infinite-scroll/js/history', 'infinite-scroll/js/button', 'infinite-scroll/js/status'], factory);
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS
    module.exports = factory(require('./core'), require('./page-load'), require('./scroll-watch'), require('./history'), require('./button'), require('./status'));
  }
})(window, function factory(InfiniteScroll) {
  return InfiniteScroll;
});
/*!
 * imagesLoaded v4.1.4
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */


(function (window, factory) {
  'use strict'; // universal module definition

  /*global define: false, module: false, require: false */

  if (typeof define == 'function' && define.amd) {
    // AMD
    define('imagesloaded/imagesloaded', ['ev-emitter/ev-emitter'], function (EvEmitter) {
      return factory(window, EvEmitter);
    });
  } else if ((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' && module.exports) {
    // CommonJS
    module.exports = factory(window, require('ev-emitter'));
  } else {
    // browser global
    window.imagesLoaded = factory(window, window.EvEmitter);
  }
})(typeof window !== 'undefined' ? window : void 0, // --------------------------  factory -------------------------- //
function factory(window, EvEmitter) {
  var $ = window.jQuery;
  var console = window.console; // -------------------------- helpers -------------------------- //
  // extend objects

  function extend(a, b) {
    for (var prop in b) {
      a[prop] = b[prop];
    }

    return a;
  }

  var arraySlice = Array.prototype.slice; // turn element or nodeList into an array

  function makeArray(obj) {
    if (Array.isArray(obj)) {
      // use object if already an array
      return obj;
    }

    var isArrayLike = _typeof(obj) == 'object' && typeof obj.length == 'number';

    if (isArrayLike) {
      // convert nodeList to array
      return arraySlice.call(obj);
    } // array of single index


    return [obj];
  } // -------------------------- imagesLoaded -------------------------- //

  /**
   * @param {Array, Element, NodeList, String} elem
   * @param {Object or Function} options - if function, use as callback
   * @param {Function} onAlways - callback function
   */


  function ImagesLoaded(elem, options, onAlways) {
    // coerce ImagesLoaded() without new, to be new ImagesLoaded()
    if (!(this instanceof ImagesLoaded)) {
      return new ImagesLoaded(elem, options, onAlways);
    } // use elem as selector string


    var queryElem = elem;

    if (typeof elem == 'string') {
      queryElem = document.querySelectorAll(elem);
    } // bail if bad element


    if (!queryElem) {
      console.error('Bad element for imagesLoaded ' + (queryElem || elem));
      return;
    }

    this.elements = makeArray(queryElem);
    this.options = extend({}, this.options); // shift arguments if no options set

    if (typeof options == 'function') {
      onAlways = options;
    } else {
      extend(this.options, options);
    }

    if (onAlways) {
      this.on('always', onAlways);
    }

    this.getImages();

    if ($) {
      // add jQuery Deferred object
      this.jqDeferred = new $.Deferred();
    } // HACK check async to allow time to bind listeners


    setTimeout(this.check.bind(this));
  }

  ImagesLoaded.prototype = Object.create(EvEmitter.prototype);
  ImagesLoaded.prototype.options = {};

  ImagesLoaded.prototype.getImages = function () {
    this.images = []; // filter & find items if we have an item selector

    this.elements.forEach(this.addElementImages, this);
  };
  /**
   * @param {Node} element
   */


  ImagesLoaded.prototype.addElementImages = function (elem) {
    // filter siblings
    if (elem.nodeName == 'IMG') {
      this.addImage(elem);
    } // get background image on element


    if (this.options.background === true) {
      this.addElementBackgroundImages(elem);
    } // find children
    // no non-element nodes, #143


    var nodeType = elem.nodeType;

    if (!nodeType || !elementNodeTypes[nodeType]) {
      return;
    }

    var childImgs = elem.querySelectorAll('img'); // concat childElems to filterFound array

    for (var i = 0; i < childImgs.length; i++) {
      var img = childImgs[i];
      this.addImage(img);
    } // get child background images


    if (typeof this.options.background == 'string') {
      var children = elem.querySelectorAll(this.options.background);

      for (i = 0; i < children.length; i++) {
        var child = children[i];
        this.addElementBackgroundImages(child);
      }
    }
  };

  var elementNodeTypes = {
    1: true,
    9: true,
    11: true
  };

  ImagesLoaded.prototype.addElementBackgroundImages = function (elem) {
    var style = getComputedStyle(elem);

    if (!style) {
      // Firefox returns null if in a hidden iframe https://bugzil.la/548397
      return;
    } // get url inside url("...")


    var reURL = /url\((['"])?(.*?)\1\)/gi;
    var matches = reURL.exec(style.backgroundImage);

    while (matches !== null) {
      var url = matches && matches[2];

      if (url) {
        this.addBackground(url, elem);
      }

      matches = reURL.exec(style.backgroundImage);
    }
  };
  /**
   * @param {Image} img
   */


  ImagesLoaded.prototype.addImage = function (img) {
    var loadingImage = new LoadingImage(img);
    this.images.push(loadingImage);
  };

  ImagesLoaded.prototype.addBackground = function (url, elem) {
    var background = new Background(url, elem);
    this.images.push(background);
  };

  ImagesLoaded.prototype.check = function () {
    var _this = this;

    this.progressedCount = 0;
    this.hasAnyBroken = false; // complete if no images

    if (!this.images.length) {
      this.complete();
      return;
    }

    function onProgress(image, elem, message) {
      // HACK - Chrome triggers event before object properties have changed. #83
      setTimeout(function () {
        _this.progress(image, elem, message);
      });
    }

    this.images.forEach(function (loadingImage) {
      loadingImage.once('progress', onProgress);
      loadingImage.check();
    });
  };

  ImagesLoaded.prototype.progress = function (image, elem, message) {
    this.progressedCount++;
    this.hasAnyBroken = this.hasAnyBroken || !image.isLoaded; // progress event

    this.emitEvent('progress', [this, image, elem]);

    if (this.jqDeferred && this.jqDeferred.notify) {
      this.jqDeferred.notify(this, image);
    } // check if completed


    if (this.progressedCount == this.images.length) {
      this.complete();
    }

    if (this.options.debug && console) {
      console.log('progress: ' + message, image, elem);
    }
  };

  ImagesLoaded.prototype.complete = function () {
    var eventName = this.hasAnyBroken ? 'fail' : 'done';
    this.isComplete = true;
    this.emitEvent(eventName, [this]);
    this.emitEvent('always', [this]);

    if (this.jqDeferred) {
      var jqMethod = this.hasAnyBroken ? 'reject' : 'resolve';
      this.jqDeferred[jqMethod](this);
    }
  }; // --------------------------  -------------------------- //


  function LoadingImage(img) {
    this.img = img;
  }

  LoadingImage.prototype = Object.create(EvEmitter.prototype);

  LoadingImage.prototype.check = function () {
    // If complete is true and browser supports natural sizes,
    // try to check for image status manually.
    var isComplete = this.getIsImageComplete();

    if (isComplete) {
      // report based on naturalWidth
      this.confirm(this.img.naturalWidth !== 0, 'naturalWidth');
      return;
    } // If none of the checks above matched, simulate loading on detached element.


    this.proxyImage = new Image();
    this.proxyImage.addEventListener('load', this);
    this.proxyImage.addEventListener('error', this); // bind to image as well for Firefox. #191

    this.img.addEventListener('load', this);
    this.img.addEventListener('error', this);
    this.proxyImage.src = this.img.src;
  };

  LoadingImage.prototype.getIsImageComplete = function () {
    // check for non-zero, non-undefined naturalWidth
    // fixes Safari+InfiniteScroll+Masonry bug infinite-scroll#671
    return this.img.complete && this.img.naturalWidth;
  };

  LoadingImage.prototype.confirm = function (isLoaded, message) {
    this.isLoaded = isLoaded;
    this.emitEvent('progress', [this, this.img, message]);
  }; // ----- events ----- //
  // trigger specified handler for event type


  LoadingImage.prototype.handleEvent = function (event) {
    var method = 'on' + event.type;

    if (this[method]) {
      this[method](event);
    }
  };

  LoadingImage.prototype.onload = function () {
    this.confirm(true, 'onload');
    this.unbindEvents();
  };

  LoadingImage.prototype.onerror = function () {
    this.confirm(false, 'onerror');
    this.unbindEvents();
  };

  LoadingImage.prototype.unbindEvents = function () {
    this.proxyImage.removeEventListener('load', this);
    this.proxyImage.removeEventListener('error', this);
    this.img.removeEventListener('load', this);
    this.img.removeEventListener('error', this);
  }; // -------------------------- Background -------------------------- //


  function Background(url, element) {
    this.url = url;
    this.element = element;
    this.img = new Image();
  } // inherit LoadingImage prototype


  Background.prototype = Object.create(LoadingImage.prototype);

  Background.prototype.check = function () {
    this.img.addEventListener('load', this);
    this.img.addEventListener('error', this);
    this.img.src = this.url; // check if image is already complete

    var isComplete = this.getIsImageComplete();

    if (isComplete) {
      this.confirm(this.img.naturalWidth !== 0, 'naturalWidth');
      this.unbindEvents();
    }
  };

  Background.prototype.unbindEvents = function () {
    this.img.removeEventListener('load', this);
    this.img.removeEventListener('error', this);
  };

  Background.prototype.confirm = function (isLoaded, message) {
    this.isLoaded = isLoaded;
    this.emitEvent('progress', [this, this.element, message]);
  }; // -------------------------- jQuery -------------------------- //


  ImagesLoaded.makeJQueryPlugin = function (jQuery) {
    jQuery = jQuery || window.jQuery;

    if (!jQuery) {
      return;
    } // set local variable


    $ = jQuery; // $().imagesLoaded()

    $.fn.imagesLoaded = function (options, callback) {
      var instance = new ImagesLoaded(this, options, callback);
      return instance.jqDeferred.promise($(this));
    };
  }; // try making plugin


  ImagesLoaded.makeJQueryPlugin(); // --------------------------  -------------------------- //

  return ImagesLoaded;
});
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function (e, t) {
  "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) ? module.exports = t() : "function" == typeof define && define.amd ? define([], t) : "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? exports.AOS = t() : e.AOS = t();
}(void 0, function () {
  return function (e) {
    function t(o) {
      if (n[o]) return n[o].exports;
      var i = n[o] = {
        exports: {},
        id: o,
        loaded: !1
      };
      return e[o].call(i.exports, i, i.exports, t), i.loaded = !0, i.exports;
    }

    var n = {};
    return t.m = e, t.c = n, t.p = "dist/", t(0);
  }([function (e, t, n) {
    "use strict";

    function o(e) {
      return e && e.__esModule ? e : {
        default: e
      };
    }

    var i = Object.assign || function (e) {
      for (var t = 1; t < arguments.length; t++) {
        var n = arguments[t];

        for (var o in n) {
          Object.prototype.hasOwnProperty.call(n, o) && (e[o] = n[o]);
        }
      }

      return e;
    },
        r = n(1),
        a = (o(r), n(6)),
        u = o(a),
        c = n(7),
        f = o(c),
        s = n(8),
        d = o(s),
        l = n(9),
        p = o(l),
        m = n(10),
        b = o(m),
        v = n(11),
        y = o(v),
        g = n(14),
        h = o(g),
        w = [],
        k = !1,
        x = document.all && !window.atob,
        j = {
      offset: 120,
      delay: 0,
      easing: "ease",
      duration: 400,
      disable: !1,
      once: !1,
      startEvent: "DOMContentLoaded"
    },
        O = function O() {
      var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
      if (e && (k = !0), k) return w = (0, y.default)(w, j), (0, b.default)(w, j.once), w;
    },
        S = function S() {
      w = (0, h.default)(), O();
    },
        _ = function _() {
      w.forEach(function (e, t) {
        e.node.removeAttribute("data-aos"), e.node.removeAttribute("data-aos-easing"), e.node.removeAttribute("data-aos-duration"), e.node.removeAttribute("data-aos-delay");
      });
    },
        E = function E(e) {
      return e === !0 || "mobile" === e && p.default.mobile() || "phone" === e && p.default.phone() || "tablet" === e && p.default.tablet() || "function" == typeof e && e() === !0;
    },
        z = function z(e) {
      return j = i(j, e), w = (0, h.default)(), E(j.disable) || x ? _() : (document.querySelector("body").setAttribute("data-aos-easing", j.easing), document.querySelector("body").setAttribute("data-aos-duration", j.duration), document.querySelector("body").setAttribute("data-aos-delay", j.delay), "DOMContentLoaded" === j.startEvent && ["complete", "interactive"].indexOf(document.readyState) > -1 ? O(!0) : "load" === j.startEvent ? window.addEventListener(j.startEvent, function () {
        O(!0);
      }) : document.addEventListener(j.startEvent, function () {
        O(!0);
      }), window.addEventListener("resize", (0, f.default)(O, 50, !0)), window.addEventListener("orientationchange", (0, f.default)(O, 50, !0)), window.addEventListener("scroll", (0, u.default)(function () {
        (0, b.default)(w, j.once);
      }, 99)), document.addEventListener("DOMNodeRemoved", function (e) {
        var t = e.target;
        t && 1 === t.nodeType && t.hasAttribute && t.hasAttribute("data-aos") && (0, f.default)(S, 50, !0);
      }), (0, d.default)("[data-aos]", S), w);
    };

    e.exports = {
      init: z,
      refresh: O,
      refreshHard: S
    };
  }, function (e, t) {},,,,, function (e, t) {
    (function (t) {
      "use strict";

      function n(e, t, n) {
        function o(t) {
          var n = b,
              o = v;
          return b = v = void 0, k = t, g = e.apply(o, n);
        }

        function r(e) {
          return k = e, h = setTimeout(s, t), S ? o(e) : g;
        }

        function a(e) {
          var n = e - w,
              o = e - k,
              i = t - n;
          return _ ? j(i, y - o) : i;
        }

        function c(e) {
          var n = e - w,
              o = e - k;
          return void 0 === w || n >= t || n < 0 || _ && o >= y;
        }

        function s() {
          var e = O();
          return c(e) ? d(e) : void (h = setTimeout(s, a(e)));
        }

        function d(e) {
          return h = void 0, E && b ? o(e) : (b = v = void 0, g);
        }

        function l() {
          void 0 !== h && clearTimeout(h), k = 0, b = w = v = h = void 0;
        }

        function p() {
          return void 0 === h ? g : d(O());
        }

        function m() {
          var e = O(),
              n = c(e);

          if (b = arguments, v = this, w = e, n) {
            if (void 0 === h) return r(w);
            if (_) return h = setTimeout(s, t), o(w);
          }

          return void 0 === h && (h = setTimeout(s, t)), g;
        }

        var b,
            v,
            y,
            g,
            h,
            w,
            k = 0,
            S = !1,
            _ = !1,
            E = !0;

        if ("function" != typeof e) throw new TypeError(f);
        return t = u(t) || 0, i(n) && (S = !!n.leading, _ = "maxWait" in n, y = _ ? x(u(n.maxWait) || 0, t) : y, E = "trailing" in n ? !!n.trailing : E), m.cancel = l, m.flush = p, m;
      }

      function o(e, t, o) {
        var r = !0,
            a = !0;
        if ("function" != typeof e) throw new TypeError(f);
        return i(o) && (r = "leading" in o ? !!o.leading : r, a = "trailing" in o ? !!o.trailing : a), n(e, t, {
          leading: r,
          maxWait: t,
          trailing: a
        });
      }

      function i(e) {
        var t = "undefined" == typeof e ? "undefined" : c(e);
        return !!e && ("object" == t || "function" == t);
      }

      function r(e) {
        return !!e && "object" == ("undefined" == typeof e ? "undefined" : c(e));
      }

      function a(e) {
        return "symbol" == ("undefined" == typeof e ? "undefined" : c(e)) || r(e) && k.call(e) == d;
      }

      function u(e) {
        if ("number" == typeof e) return e;
        if (a(e)) return s;

        if (i(e)) {
          var t = "function" == typeof e.valueOf ? e.valueOf() : e;
          e = i(t) ? t + "" : t;
        }

        if ("string" != typeof e) return 0 === e ? e : +e;
        e = e.replace(l, "");
        var n = m.test(e);
        return n || b.test(e) ? v(e.slice(2), n ? 2 : 8) : p.test(e) ? s : +e;
      }

      var c = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (e) {
        return _typeof(e);
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : _typeof(e);
      },
          f = "Expected a function",
          s = NaN,
          d = "[object Symbol]",
          l = /^\s+|\s+$/g,
          p = /^[-+]0x[0-9a-f]+$/i,
          m = /^0b[01]+$/i,
          b = /^0o[0-7]+$/i,
          v = parseInt,
          y = "object" == ("undefined" == typeof t ? "undefined" : c(t)) && t && t.Object === Object && t,
          g = "object" == ("undefined" == typeof self ? "undefined" : c(self)) && self && self.Object === Object && self,
          h = y || g || Function("return this")(),
          w = Object.prototype,
          k = w.toString,
          x = Math.max,
          j = Math.min,
          O = function O() {
        return h.Date.now();
      };

      e.exports = o;
    }).call(t, function () {
      return this;
    }());
  }, function (e, t) {
    (function (t) {
      "use strict";

      function n(e, t, n) {
        function i(t) {
          var n = b,
              o = v;
          return b = v = void 0, O = t, g = e.apply(o, n);
        }

        function r(e) {
          return O = e, h = setTimeout(s, t), S ? i(e) : g;
        }

        function u(e) {
          var n = e - w,
              o = e - O,
              i = t - n;
          return _ ? x(i, y - o) : i;
        }

        function f(e) {
          var n = e - w,
              o = e - O;
          return void 0 === w || n >= t || n < 0 || _ && o >= y;
        }

        function s() {
          var e = j();
          return f(e) ? d(e) : void (h = setTimeout(s, u(e)));
        }

        function d(e) {
          return h = void 0, E && b ? i(e) : (b = v = void 0, g);
        }

        function l() {
          void 0 !== h && clearTimeout(h), O = 0, b = w = v = h = void 0;
        }

        function p() {
          return void 0 === h ? g : d(j());
        }

        function m() {
          var e = j(),
              n = f(e);

          if (b = arguments, v = this, w = e, n) {
            if (void 0 === h) return r(w);
            if (_) return h = setTimeout(s, t), i(w);
          }

          return void 0 === h && (h = setTimeout(s, t)), g;
        }

        var b,
            v,
            y,
            g,
            h,
            w,
            O = 0,
            S = !1,
            _ = !1,
            E = !0;

        if ("function" != typeof e) throw new TypeError(c);
        return t = a(t) || 0, o(n) && (S = !!n.leading, _ = "maxWait" in n, y = _ ? k(a(n.maxWait) || 0, t) : y, E = "trailing" in n ? !!n.trailing : E), m.cancel = l, m.flush = p, m;
      }

      function o(e) {
        var t = "undefined" == typeof e ? "undefined" : u(e);
        return !!e && ("object" == t || "function" == t);
      }

      function i(e) {
        return !!e && "object" == ("undefined" == typeof e ? "undefined" : u(e));
      }

      function r(e) {
        return "symbol" == ("undefined" == typeof e ? "undefined" : u(e)) || i(e) && w.call(e) == s;
      }

      function a(e) {
        if ("number" == typeof e) return e;
        if (r(e)) return f;

        if (o(e)) {
          var t = "function" == typeof e.valueOf ? e.valueOf() : e;
          e = o(t) ? t + "" : t;
        }

        if ("string" != typeof e) return 0 === e ? e : +e;
        e = e.replace(d, "");
        var n = p.test(e);
        return n || m.test(e) ? b(e.slice(2), n ? 2 : 8) : l.test(e) ? f : +e;
      }

      var u = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (e) {
        return _typeof(e);
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : _typeof(e);
      },
          c = "Expected a function",
          f = NaN,
          s = "[object Symbol]",
          d = /^\s+|\s+$/g,
          l = /^[-+]0x[0-9a-f]+$/i,
          p = /^0b[01]+$/i,
          m = /^0o[0-7]+$/i,
          b = parseInt,
          v = "object" == ("undefined" == typeof t ? "undefined" : u(t)) && t && t.Object === Object && t,
          y = "object" == ("undefined" == typeof self ? "undefined" : u(self)) && self && self.Object === Object && self,
          g = v || y || Function("return this")(),
          h = Object.prototype,
          w = h.toString,
          k = Math.max,
          x = Math.min,
          j = function j() {
        return g.Date.now();
      };

      e.exports = n;
    }).call(t, function () {
      return this;
    }());
  }, function (e, t) {
    "use strict";

    function n(e, t) {
      a.push({
        selector: e,
        fn: t
      }), !u && r && (u = new r(o), u.observe(i.documentElement, {
        childList: !0,
        subtree: !0,
        removedNodes: !0
      })), o();
    }

    function o() {
      for (var e, t, n = 0, o = a.length; n < o; n++) {
        e = a[n], t = i.querySelectorAll(e.selector);

        for (var r, u = 0, c = t.length; u < c; u++) {
          r = t[u], r.ready || (r.ready = !0, e.fn.call(r, r));
        }
      }
    }

    Object.defineProperty(t, "__esModule", {
      value: !0
    });
    var i = window.document,
        r = window.MutationObserver || window.WebKitMutationObserver,
        a = [],
        u = void 0;
    t.default = n;
  }, function (e, t) {
    "use strict";

    function n(e, t) {
      if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
    }

    function o() {
      return navigator.userAgent || navigator.vendor || window.opera || "";
    }

    Object.defineProperty(t, "__esModule", {
      value: !0
    });

    var i = function () {
      function e(e, t) {
        for (var n = 0; n < t.length; n++) {
          var o = t[n];
          o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, o.key, o);
        }
      }

      return function (t, n, o) {
        return n && e(t.prototype, n), o && e(t, o), t;
      };
    }(),
        r = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i,
        a = /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i,
        u = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i,
        c = /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i,
        f = function () {
      function e() {
        n(this, e);
      }

      return i(e, [{
        key: "phone",
        value: function value() {
          var e = o();
          return !(!r.test(e) && !a.test(e.substr(0, 4)));
        }
      }, {
        key: "mobile",
        value: function value() {
          var e = o();
          return !(!u.test(e) && !c.test(e.substr(0, 4)));
        }
      }, {
        key: "tablet",
        value: function value() {
          return this.mobile() && !this.phone();
        }
      }]), e;
    }();

    t.default = new f();
  }, function (e, t) {
    "use strict";

    Object.defineProperty(t, "__esModule", {
      value: !0
    });

    var n = function n(e, t, _n) {
      var o = e.node.getAttribute("data-aos-once");
      t > e.position ? e.node.classList.add("aos-animate") : "undefined" != typeof o && ("false" === o || !_n && "true" !== o) && e.node.classList.remove("aos-animate");
    },
        o = function o(e, t) {
      var o = window.pageYOffset,
          i = window.innerHeight;
      e.forEach(function (e, r) {
        n(e, i + o, t);
      });
    };

    t.default = o;
  }, function (e, t, n) {
    "use strict";

    function o(e) {
      return e && e.__esModule ? e : {
        default: e
      };
    }

    Object.defineProperty(t, "__esModule", {
      value: !0
    });

    var i = n(12),
        r = o(i),
        a = function a(e, t) {
      return e.forEach(function (e, n) {
        e.node.classList.add("aos-init"), e.position = (0, r.default)(e.node, t.offset);
      }), e;
    };

    t.default = a;
  }, function (e, t, n) {
    "use strict";

    function o(e) {
      return e && e.__esModule ? e : {
        default: e
      };
    }

    Object.defineProperty(t, "__esModule", {
      value: !0
    });

    var i = n(13),
        r = o(i),
        a = function a(e, t) {
      var n = 0,
          o = 0,
          i = window.innerHeight,
          a = {
        offset: e.getAttribute("data-aos-offset"),
        anchor: e.getAttribute("data-aos-anchor"),
        anchorPlacement: e.getAttribute("data-aos-anchor-placement")
      };

      switch (a.offset && !isNaN(a.offset) && (o = parseInt(a.offset)), a.anchor && document.querySelectorAll(a.anchor) && (e = document.querySelectorAll(a.anchor)[0]), n = (0, r.default)(e).top, a.anchorPlacement) {
        case "top-bottom":
          break;

        case "center-bottom":
          n += e.offsetHeight / 2;
          break;

        case "bottom-bottom":
          n += e.offsetHeight;
          break;

        case "top-center":
          n += i / 2;
          break;

        case "bottom-center":
          n += i / 2 + e.offsetHeight;
          break;

        case "center-center":
          n += i / 2 + e.offsetHeight / 2;
          break;

        case "top-top":
          n += i;
          break;

        case "bottom-top":
          n += e.offsetHeight + i;
          break;

        case "center-top":
          n += e.offsetHeight / 2 + i;
      }

      return a.anchorPlacement || a.offset || isNaN(t) || (o = t), n + o;
    };

    t.default = a;
  }, function (e, t) {
    "use strict";

    Object.defineProperty(t, "__esModule", {
      value: !0
    });

    var n = function n(e) {
      for (var t = 0, n = 0; e && !isNaN(e.offsetLeft) && !isNaN(e.offsetTop);) {
        t += e.offsetLeft - ("BODY" != e.tagName ? e.scrollLeft : 0), n += e.offsetTop - ("BODY" != e.tagName ? e.scrollTop : 0), e = e.offsetParent;
      }

      return {
        top: n,
        left: t
      };
    };

    t.default = n;
  }, function (e, t) {
    "use strict";

    Object.defineProperty(t, "__esModule", {
      value: !0
    });

    var n = function n(e) {
      e = e || document.querySelectorAll("[data-aos]");
      var t = [];
      return [].forEach.call(e, function (e, n) {
        t.push({
          node: e
        });
      }), t;
    };

    t.default = n;
  }]);
});
"use strict";

require("infinite-scroll");
"use strict";

var _lazysizes = _interopRequireDefault(require("lazysizes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
"use strict";
<?php
/**
 * The main template file
 *
 * @package caffeinebuilt
 */

?>

<header class="section__opening page__opening">
	<div class="section__opening--inner">
		<h1>
			<span class="heading-bg">
				<?php single_post_title(); ?>
			</span>
		</h1>
	</div>
</header>

<?php
if ( have_posts() ) :

	while ( have_posts() ) : the_post();

		get_template_part( 'template-parts/content', get_post_format() );

	endwhile;

	the_posts_navigation();

else :

	get_template_part( 'template-parts/content', 'none' );

endif;
?>
<?php //get_sidebar(); ?>
